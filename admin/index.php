<?php
session_start();

$config = require ('config.php');

$errors = [
	'err' => false,
	'msg' => ''
];

 if ($_POST) {
	 //  SI SE CONECTO Y SI SE ENVIARON AMBOS DATOS SE PROCEDE CON LA CONSULTA DE EXISTENCIA DEL USUARIO EVITANDO INYECCIONES SQL
	try {
		//Conexión PDO a Base de datos
		$conn = new PDO($config['db']['conn'], $config['db']['user'], $config['db']['pass'], $config['db']['options']);
		$stmt = $conn->prepare('SELECT * from users WHERE username = ?');
		$stmt->execute(array( $_POST['username'] ));

		//Valida si hay resultados
		if ($stmt->rowCount() > 0){
				$result = $stmt->fetch();
				//Valida si la contraseña suministrada es igual a la almacenada
				if(password_verify( $_POST['password'], $result['password'])){
					session_regenerate_id();
					$_SESSION['loggedin'] = TRUE;
					$_SESSION['username'] = $_POST['username'];
					$_SESSION['name'] = $_POST['name'];
					header('Location: panel.php');
					
				} else {
					// Contraseña incorrecta
					$errors['err'] = true;
					$errors['msg'] = 'Contrasñea incorrecta.';
				}
			} else {
				//El usuario ingresado no existe.
				$errors['err'] = true;
				$errors['msg'] = 'Usuario incorrecto.';
		}
	} catch (PDOException $e) {
		$errors['err'] = true;
		$errors['msg'] = $e->getMessage();
		die();
	}
 }
?>

<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Inicio de sesión | <?= $config['site']['name'] ?></title>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style-login.css">
	</head>
	<body class="align">
		<div class="grid">
			<?php
			if ($errors['err']) {
			?>
				<div class="alert alert-danger text-center" role="alert">
					<?= $errors['msg'] ?>
				</div>
			<?php
			}
			?>
			<form method="POST" class="form login">
                <div class="form__field logo">
                    <figure>
                        <img src="../images/logo-insdeportes-bl.png" alt="">
                    </figure>
                </div>

				<div class="form__field">
					<label for="login__username">
                        <svg class="icon"><use xlink:href="#icon-user"></use></svg>
                        <span class="hidden">Usuario</span>
                    </label>
					<input autocomplete="username" id="login__username" type="text" name="username" class="form__input" placeholder="Usuario" required/>
				</div>

				<div class="form__field">
					<label for="login__password">
                        <svg class="icon"><use xlink:href="#icon-lock"></use></svg>
                        <span class="hidden">Contraseña</span>
                    </label>
					<input id="login__password" type="password" name="password" class="form__input" placeholder="Contraseña" required />
				</div>

				<div class="form__field">
					<input type="submit" value="Iniciar sesión" />
				</div>
			</form>

			<p class="text--center">
				¿No tiene cuenta? Comuniquese con un <a href="#">administrador</a>.
			</p>
		</div>

		<svg xmlns="http://www.w3.org/2000/svg" class="icons">
			<symbol id="icon-lock" viewBox="0 0 1792 1792">
				<path d="M640 768h512V576q0-106-75-181t-181-75-181 75-75 181v192zm832 96v576q0 40-28 68t-68 28H416q-40 0-68-28t-28-68V864q0-40 28-68t68-28h32V576q0-184 132-316t316-132 316 132 132 316v192h32q40 0 68 28t28 68z"/>
			</symbol>
			<symbol id="icon-user" viewBox="0 0 1792 1792">
				<path d="M1600 1405q0 120-73 189.5t-194 69.5H459q-121 0-194-69.5T192 1405q0-53 3.5-103.5t14-109T236 1084t43-97.5 62-81 85.5-53.5T538 832q9 0 42 21.5t74.5 48 108 48T896 971t133.5-21.5 108-48 74.5-48 42-21.5q61 0 111.5 20t85.5 53.5 62 81 43 97.5 26.5 108.5 14 109 3.5 103.5zm-320-893q0 159-112.5 271.5T896 896 624.5 783.5 512 512t112.5-271.5T896 128t271.5 112.5T1280 512z"/>
			</symbol>
		</svg>
	</body>
</html>
