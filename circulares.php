<?php
	$title = 'Circulares' ;
	require ('template/header.php');
?>

	<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
		<div class="container">
			<h1>Circulares</h1>
		</div>
	</section>

	<section class="bg-breadcrumbs bg-light text-center text-sm-left">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="/">Inicio</a></li>
				<li><a class="text-dark">Circulares /</a></li>
			</ol>
		</div>
	</section>

	<section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<article class="post-blog">
						<div class="post-img">
							<img alt="" src="images/documentos3.jpg"/>
						</div>
						<div class="post-meta-top">
							<time datetime="2016"><a class="text-base" href="#">Diciembre, 2020</a></time><span>&nbsp; Manejo en Escenarios</span>
						</div>
						<div class="post-title">
							<h5 class="text-regular">
								Remitente: Directora Insdeportes
							</h5>
						</div>
						<div class="post-body">
							<p>
								Circular No.22 Manejo en escenarios
								Parque las Ramplas y escenario Mario
								Don Oso.
							</p>
						</div>
					</article>
				</div>
				<div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
					<div class="sidebar text-xs-left">
						<div class="sidebar-module">
							<h5>A&ntilde;o 2022</h5>
							<ul class="list-unordered-variant-3">
								<li>
									<a class="text-ebold" href="circular-2022" title="Circulares del año 2022" >Ver archivos</a>
								</li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2021</h5>

							<ul class="list-unordered-variant-3">
								<li>
									<p>
										<a
											class="text-ebold"
											href="files/circulares/2021/021 CIRCULAR VACACIONES RECREATIVAS 2021.pdf"
											target="_blank"
											title="Inscripciones 1-2021"
											>CIRCULAR N&deg;021</a
										>
									</p>
								</li>
								<li>
									<p>
										<a
											class="text-ebold"
											href="files/circulares/2021/025 CIRCULAR INFORMATIVA RODANDO AL 100.pdf"
											target="_blank"
											title="Inscripciones 1-2021"
											>CIRCULAR N&deg;029</a
										>
									</p>
								</li>
								<li>
									<a
										class="text-ebold"
										href="files/circulares/004_inscripciones_2021.pdf"
										target="_blank"
										title="Inscripciones 1-2021"
										>CIRCULAR N&deg;004</a
									>
								</li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2020</h5>
							<ul class="list-unordered-variant-3">
								<li>
									<a
										class="text-ebold"
										href="files/circulares/n22.PDF"
										title="11 de Diciembre de 2020"
										>CIRCULAR N&deg;22</a
									>
								</li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2019</h5>
							<ul class="list-unordered-variant-3"></ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2018</h5>
							<ul class="list-unordered-variant-3"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
    require ('template/footer.php');