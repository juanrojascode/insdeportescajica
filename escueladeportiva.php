<?php
    $title = 'Escuela Deportiva' ;
    require ('template/header.php');
?>

	<section class="section-55 section-lg-top-115 section-lg-bottom-125 text-sm-left">
		<div class="container">
			<h1 style="text-align: center;">Escuela Deportiva</h1>

			<div class="row offset-top-40">
				<div class="col-xs-12">
					<div class="responsive-tabs responsive-accordion-variant-1" data-type="accordion">
						<ul class="resp-tabs-list">
							<li class="h5 text-regular">Deporte Asociado Competitivo,&nbsp;Social&nbsp;Comunitario, Formativo y &Eacute;lite&nbsp;</li>
							<li class="h5 text-regular">Resultados</li>
							<li class="h5 text-regular">Paz y Convivencia</li>
							<li class="h5 text-regular"><u><a href="https://playsync.app/" target="_blank"><span style="color:#FFFFFF;">Inscripciones</span></a></u></li>
						</ul>

						<div class="resp-tabs-container">
							<div>
								<ul>
									<li><a href="clubes" target="_blank">Deporte Asociado y Competitivo</a></li>
									<li><a href="proximamente" target="_blank">Deporte Social y Comunitario</a></li>
									<li><a href="proximamente" target="_blank">Deporte Formativo</a></li>
									<li><a href="proximamente" target="_blank">Deporte &Eacute;lite</a></li>
									<li></li>
								</ul>
							</div>

							<div>
								<ul>
									<li><a href="resultados_deportivos" target="_blank">Resultados Deportivos</a> <!-- RD Navbar Dropdown--></li>
								</ul>
							</div>

							<div>
								<ul>
									<li class="h6 text-ebold text-base text-uppercase"><a href="files/paz.pdf" target="_blank">Paz y Convivencia</a></li>
								</ul>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
    require ('template/footer.php');