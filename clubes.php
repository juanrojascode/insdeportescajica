<?php
    $title = 'Clubes' ;
    require ('template/header.php');
?>

  <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
    <div class="container">
      <h1 style="text-align: center;">Deporte Asociado y Competitivo&nbsp;</h1>

      <p style="text-align: justify;">El &aacute;rea de deporte asociado y competitivo tiene a su cargo el proceso de asesorar y brindar acompa&ntilde;amiento a los clubes deportivos con el fin de otorgar, renovar o actualizar sus reconocimientos deportivos, as&iacute; mismo se generan espacios competitivos desarrollando eventos del nivel federado en la cual Cajic&aacute; pueda ser sede y de esta forma fomentar las disciplinas deportivas en todos los niveles a nivel competitivo. De otra parte, se gestiona el pr&eacute;stamo o alquiler de los escenarios deportivos a cargo del Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute;.</p>
    </div>
  </section>
  <section class="bg-breadcrumbs bg-light text-center text-sm-left">
    <div class="container">
      <ol class="breadcrumb">
        <li style="text-align: justify;"><a href="/">Inicio</a></li>
        <li>Deporte Asociado y Competitivo</li>
        <li class="active">Clubes</li>
      </ol>

      <p style="text-align: justify;"></p>
    </div>
  </section>
  <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left timeline">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-left text-md-center">
          <time class="h5 meta-timeline" datetime="2015">A&ntilde;o 2021</time>
        </div>

        <div class="col-xs-12 col-md-5 offset-top-30">
          <article class="post-blog post-blog-mod-1 view-animate">
            <div class="post-img"><img alt="" src="images/club1.jpg" /></div>
            <div class="post-meta-top"><time datetime="2016"><a class="text-base">Enero, 2021</a></time><span>&nbsp; &nbsp;</span></div>
            <div class="post-title">
              <h5 class="text-regular"><a class="text-base">CLUBES DEPORTIVOS VIGENTES</a></h5>
            </div>
            <div class="post-body">
              <p>El Club Deportivo de Patinaje FORTALEZA fue fundado el d&iacute;a 10 de Julio de 2013 en el municipio de Cajica del departamento de Cundinamarca, es una entidad de derecho privado sin &aacute;nimo de lucro, constituido con el fin de fomentar y patrocinar la pr&aacute;ctica del deporte del patinaje de velocidad, como modalidad competitiva, recreativa y aprovechamiento del tiempo libre en el municipio, e impulsar programas de inter&eacute;s p&uacute;blico y social con deportistas aficionados y padres que amen este lindo deporte.</p>
              <p>Direcci&oacute;n Sede Administrativa: Cra 4 N&deg;4-77 CAJICA.<br />
              <br />
              Direccion Sede Deportiva: Pista de Patinaje de Cajic&aacute;.<br />
              <br />
              E-mail: clubfortalezadepatinaje@gmail.com.</p>
              <dl class="dl-horizontal-variant-1 clearfix">
                <dt></dt>
                <dd><a class="text-bold text-base h6" href="callto:#">3106254860 /3202126511</a></dd>
              </dl>
            </div>
            <div class="post-meta-bottom">
              <ul class="meta-categories">
                <li><a a="" href="files/club/CALENDARIO-CARRERAS-2020-2021.pdf">Calendario Carreras 2020-2021 </a>
                <li><a a="" href="https://www.facebook.com/fortaleza.cajica.5">Redes Sociales </a></li>
                </li>
              </ul>
            </div>
          </article>
        </div>

        <div class="col-xs-12 col-md-5 col-md-offset-2 inset-1 timeline-right offset-top-50 offset-md-top-160">
          <article class="post-blog post-blog-mod-1 view-animate">
            <div class="post-img"><a a="" href="https://www.facebook.com/fortaleza.cajica.5"><img alt="" src="images/club2.jpg" /></a></div>
          </article>
        </div>

        <div class="col-xs-12 col-md-5 col-md-offset-2 inset-1 timeline-right offset-top-50 offset-md-top-160">
        <article class="post-blog post-blog-mod-1 view-animate">
          <div class="post-img">
            <a a="" href="https://www.facebook.com/pages/category/Martial-Arts-School/Club-de-Hapkido-Lao-Tse-Cajic%C3%A1-102370061472688"><img alt="" src="images/logo_hapkido.png" /> </a>
          </div>
          <div class="post-meta-top"><a class="text-base">Marzo, 2021</a><span>&nbsp; &nbsp;</span></div>
          <div class="post-title">
            <h5 class="text-regular"><a class="text-base">CLUB DE HAPKIDO LAO TSE CAJICA</a></h5>
          </div>
          <div class="post-body">
            <p>El Club de hapkido LAO TSE Cajic&aacute; es fundado el 20 de Octubre de 2018 con el fin de fomentar y patrocinar la pr&aacute;ctica del hapkido, la recreaci&oacute;n y el aprovechamiento del tiempo libre e impulsar programas de inter&eacute;s p&uacute;blico y social,donde los ni&ntilde;os podr&aacute;n iniciar la actividad f&iacute;sica y mejorar sus capacidades motoras y de coordinaci&oacute;n. Los j&oacute;venes podr&aacute;n explorar todas sus capacidades f&iacute;sicas y desarrollar al m&aacute;ximo su potencial en cuanto a actividad f&iacute;sica y deportiva se refiere y aprender t&eacute;cnicas que les ayuden en la defensa personal. Los adultos podr&aacute;n mantenerse en buenas condiciones f&iacute;sicas y de salud al mismo tiempo que desarrollan sus capacidades en defensa personal.</p>
            <p>Direcci&oacute;n: Carrera 14b no 3-45 Cajic&aacute;.</p>
            
            <p>Direcci&oacute;n lugar entrenamiento deportivo: Calle 10a #7-42 este.<br />
            <br />
            Barrio :Puente Vargas- Cajic&aacute; .<br />
            <br />
            E-mail: laotsecajica@gmail.com.</p>
            
            <dl class="dl-horizontal-variant-1 clearfix">
              <dt></dt>
              <dd><a class="text-bold text-base h6" href="callto:#">3208002554 /3108701590 </a></dd>
            </dl>
          </div>
          <div class="post-meta-bottom">
            <ul class="meta-categories">
              <li><a a="" href="https://www.facebook.com/pages/category/Martial-Arts-School/Club-de-Hapkido-Lao-Tse-Cajic%C3%A1-102370061472688/">Redes Sociales </a></li>
            </ul>
          </div>
        </article>
        </div>
      </div>
    </div>
  </section>

<?php
  require ('template/footer.php');