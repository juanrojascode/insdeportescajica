<?php
    $title = 'Planes del 2021' ;
    require ('template/header.php');
?>
    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Planes y Programas</h1>
            <!-- <h1>Planes y Programas<span> A&ntilde;o 2021 /</span></h1> -->
        </div>
    </section>
    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="planes" class="text-dark">Planes y Programas</a></li>
                <li><a class="text-dark">2021</a></li>
            </ol>
        </div>
    </section>
    <!--section blog-->

    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left planes">
        <div class="container py-3">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">CODIGO DE INTEGRIDAD</h4>
                            <a href="/files/planes/CODIGO DE INTEGRIDAD.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANTICORRUPCION Y ATENCION CIUDADANA</h4>
                            <a href="/files/planes/PLAN ANTICORRUPCION Y ATENCION CIUDADANA.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANUAL DE TRABAJO EN SST CONSOLIDADO</h4>
                            <a href="/files/planes/PLAN ANUAL DE TRABAJO EN SST CONSOLIDADO.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ESTRATEGICO DE TALENTO HUMANO</h4>
                            <a href="/files/planes/PLAN ESTRATEGICO DE TALENTO HUMANO.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN INSTITUCIONAL DE ARCHIVO</h4>
                            <a href="/files/planes/PLAN INSTITUCIONAL DE ARCHIVO.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE ACCION</h4>
                            <a href="/files/planes/PLAN DE ACCION.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE BIENESTAR E INCENTIVOS FINAL</h4>
                            <a href="/files/planes/PLAN DE BIENESTAR E INCENTIVOS FINAL.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE CAPACITACIÓN FINAL</h4>
                            <a href="/files/planes/PLAN DE CAPACITACIOìN FINAL.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE TRATAMIENTO DE  RIESGOS DE SEGURIDAD Y PRIVACIDAD DE LA INFORMACIÓN</h4>
                            <a href="/files/planes/PLAN DE TRATAMIENTO DE  RIESGOS DE SEGURIDAD Y PRIVACIDAD DE LA INFORMACIOìN.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PREVISIÓN Y VACANTES DE RECURSOS HUMANOS</h4>
                            <a href="/files/planes/Prevision y vacantes de Recursos Humanos.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">RESOLUCIÓN PLAN ANUAL DE AQUISICIONES 2021 FINAL</h4>
                            <a href="/files/planes/resolucion plan anual de adquisiciones  2021FINAL.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?= $config['site']['icon-pdf'] ?>">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">RESOLUCIÓN NO. 005 - APROBACIÓN PLAN ANUAL MENSUALIDAD DE CAJA-PAC</h4>
                            <a href="/files/planes/RES 005 PAC 2021.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Footer-->
<?php
    require ('template/footer.php');