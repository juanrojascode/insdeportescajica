<?php
    $title = 'Protocolos de bioseguridad' ;
    require ('template/header.php');
?>

    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Protocolos de bioseguridad</h1>
        </div>
    </section>

    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="bienestar" class="text-dark">Bienestar</a></li>
                <li class="active">Bioseguridad</li>
            </ol>
        </div>
    </section>

    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="post-blog post-blog-mod-2">
                        <div class="post-body">
                            <p>El presente documento tiene como objetivo establecer los protocolos mínimos de operación en los Centros de Trabajo del <b>INSTITUTO MUNICIPAL DE DEPORTES Y RECREACIÓN DE CAJICÁ</b>, con el fin de brindar todas las garantías de bioseguridad a los funcionarios, contratistas, usuarios y visitantes, para la re activación de actividades de forma presencial con el fin de reducir la posibilidad de contagio por Covid-19, dentro de la emergencia sanitaria actual</p>
                            <p class="text-center">
                                <a href="files/protocolodebioseguridadInsdeporres2022.pdf"><img src="<?= $config['site']['icon-pdf'] ?>" alt="Icono de PDF" class="icon-pdf ms-0">Protocolo de bioseguridad Insdeportes</a>
                                <a href="files/capacitacion-covid19-informacioonalosusuarios.pdf"><img src="<?= $config['site']['icon-pdf'] ?>" alt="Icono de PDF" class="icon-pdf">Capacitación - COVID 19</a>
                            </p>
                            <div class="row" style="display: flex; justify-content: center; margin-top: 1rem">
                                <div class="col-xs-8">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/09OEeDdkHrs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    require ('template/footer.php');