<?php
    $title = 'Transparencia' ;
    require ('template/header.php');
?>

    <main class="page-content">
        <section class="section-55 section-lg-top-115 section-lg-bottom-0 text-sm-left">
            <div class="container">
                <h1 style="text-align: center">Transparencia y Acceso a la Información</h1>

                <p>De acuerdo a la <a data-wahfont="15" href="http://wsp.presidencia.gov.co/Normativa/Leyes/Documents/LEY%201712%20DEL%2006%20DE%20MARZO%20DE%202014.pdf" rel="noopener noreferrer" target="_blank">Ley 1712 de 2014</a> y a la <a data-wahfont="15" href="https://www.cajica.gov.co/docdown/archi/2021/Resolucion/Resoluci%C3%B3n%201519%20de%202020.pdf" rel="noopener noreferrer" target="_blank">Resolución 1519 de 2020</a>, el Instituto Municipal de Deporte y Recreación de Cajicá pone a disposición de los ciudadanos la nueva sección de Transparencia y Acceso a la Información Pública Nacional, donde podrán conocer de primera mano la información del instituto.</p>

                <p style="text-align: center">
                    <iframe allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" frameborder="0" height="315" src="https://www.youtube.com/embed/HS-JetzO_Aw" title="YouTube video player" width="560"></iframe>
                </p>

                <div class="row offset-top-40">
                    <div class="col-xs-12">
                        <div class="responsive-tabs responsive-accordion-variant-1" data-type="accordion">
                            <ul class="resp-tabs-list">
                                <li class="h5 text-regular">
                                    Información de la Entidad
                                </li>
                                <li class="h5 text-regular">
                                    Planeación, presupuestos e informes
                                </li>
                                <li class="h5 text-regular">
                                    Datos Abiertos
                                </li>
                                <li class="h5 text-regular">
                                    Actos Administrativos, resoluciones, circulares y notificaciones
                                </li>
                                <li class="h5 text-regular">
                                    Políticas
                                </li>
                                <li class="h5 text-regular">
                                    Política pública del deporte y la recreación
                                </li>
                            </ul>
                            <div class="resp-tabs-container">
                                <div>
                                    <ul>
                                        <li>
                                            <a href="mision" target="_blank">Misión, Visión, Políticas y Valores</a>
                                        </li>
                                        <li>
                                            <a href="our-team" target="_blank">Equipo de Trabajo</a>
                                        </li>
                                        <li>
                                            <a href="files/MAPA-DE-PROCESOS-INSDEPORTES.jpg" target="_blank">Estructura Organizacional</a>
                                        </li>
                                        <li>
                                            <a href="#">Imagen Institucional</a>
                                        </li>
                                        <li>
                                            <a href="files/ORGANIGRAMA.png" target="_blank">Organigrama</a>
                                        </li>
                                        <li>
                                            <a href="files/PLAN ESTRATEGICO DE TALENTO HUMANO.pdf" target="_blank">Plan Estratégico de Talento Humano</a>
                                        </li>
                                        <li>
                                            <span style="color: #000000"><u><strong>Alcaldía de Cajicá</strong></u></span>
                                        </li>
                                        <li>
                                            <a href="files/PROGRAMA-DE-GOBIERNO-2020-2013.pdf" target="_blank">Programa de Gobierno</a>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <ul>
                                        <li>
                                            <span style="color: #000000"><u><strong>Plán Anticorrupción</strong></u></span>
                                            <ul>
                                                <li>
                                                    <a href="files/planes/PLAN ANTICORRUPCION Y ATENCION CIUDADANA.pdf" target="_blank">2021</a>
                                                </li>
                                                <li>
                                                    <a href="files/Resolucion-No.-41-de-2020-Adopcion-.pdf" target="_blank">2020</a>
                                                </li>
                                                <li>
                                                    <a href="files/plan-anti-corrupcion-2019.pdf" target="_blank">2019</a>
                                                </li>
                                                <li>
                                                    <a href="files/PLAN-ANTICORRUPCION-Y-ATENCION-CIUDADANA-2018.pdf" target="_blank">2018</a>
                                                </li>
                                                <li>
                                                    <a href="files/PLAN-ANTICORRUPCION-2017.pdf" target="_blank">2017</a>
                                                </li>
                                                <li>
                                                    <a href="files/PLANANTICORRUPCION2016.pdf" target="_blank">2016</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="planes" target="_blank">Planes y Programas</a>
                                        </li>
                                        <li>
                                            <a href="#">Presupuestos</a>
                                        </li>
                                        <li>
                                            <a href="rendicion" target="_blank">Rendición de Cuentas</a>
                                        </li>
                                        <li>
                                            <a href="informes" target="_blank">Estados Financieros</a>
                                        </li>
                                        <li>
                                            <span style="color: #000000"><u><strong>Austeridad del Gasto</strong></u></span>
                                            <ul>
                                                <li>
                                                    <a href="files/INFORME-DE-AUSTERIDAD-DEL-GASTO-DICIEMBRE-2019.pdf" target="_blank">Informe Austeridad del Gasto Septiembre-Diciembre 2019</a>
                                                </li>
                                                <li>
                                                    <a href="files/INFORME-DE-AUSTERIDAD-DEL-GASTO-SEPTIEMBRE-2019.pdf" target="_blank">Informe Austeridad del Gasto Septiembre 2019</a>
                                                </li>
                                                <li>
                                                    <a href="files/INFORME-DE-AUSTERIDAD-DEL-GASTO-MARZO-2019.pdf" target="_blank">Informe Austeridad del Gasto Enero-Marzo 2019</a>
                                                </li>
                                                <li>
                                                    <a href="files/INFORME-DE-AUSTERIDAD-DEL-GASTO-JUNIO-2019.pdf" target="_blank">Informe Austeridad del Gasto Enero-Junio 2019</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="files/5.-ACTA-DE-EMPALME-AREA-FINACIERA-Y-ALMACEN.pdf" target="_blank">Empalme 2019 - 2020</a>
                                        </li>
                                        <li>
                                            <a href="rendicion" target="_blank">Rendición de Cuentas</a>
                                        </li>
                                        <li>
                                            <u><strong><a><span style="color: #000000;">Control Interno</span></a></strong></u>
                                            <ul>
                                                <li>
                                                    <a href="files/INFORMECISEGUNDOSEMESTRE2021.pdf" target="_blank">Informe 2do Periodo 2021</a>
                                                </li>
                                                <li>
                                                    <a href="files/CONTROL INTERNO INFORME SEMESTRAL 1 PERIODO 2021.pdf" target="_blank">Informe 1er Periodo 2021</a>
                                                </li>
                                                <li>
                                                    <a href="files/INFORME C.I. SEGUNDO SEMESTRE 2020 FINAL.pdf" target="_blank">Informe 2do Periodo 2020</a>
                                                </li>
                                                <li>
                                                    <a href="files/Informe_1_semestre_2020.pdf" target="_blank">Informe 1er Periodo 2020</a>
                                                </li>
                                                <li>
                                                    <a href="files/INFORME-PORMENORIZADO-DE-CONTROL-INTERNO-1-2019.pdf" target="_blank">Informe Pormenorizados 2019</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <ul>
                                        <li>
                                            <a href="#">Gestión documental</a>
                                        </li>
                                        <li>
                                            <a href="recursos">Recursos</a>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <ul>
                                        <li>
                                            <a href="archivos">Actos Administrativos, Resoluciones e Informativos</a>
                                        </li>
                                        <li>
                                            <a href="circulares">Circulares</a>
                                        </li>
                                        <li>
                                            <a href="">Notificaciones por aviso</a>
                                        </li>
                                        <li>
                                            <a href="comunicados">Comunicados</a>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <ul>
                                        <li>
                                            <a href="#">Seguridad de la Información</a>
                                        </li>
                                        <li>
                                            <a href="files/POLITICA-DE-SEGURIDAD.pdf" target="_blank">Seguridad y Salud en el Trabajo</a>
                                        </li>
                                    </ul>
                                </div>

                                <div>
                                    <!-- <ul>
                                        <li>
                                            <a href="#">Lorem</a>
                                        </li>
                                        <li>
                                            <a href="#">Lorem</a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

<?php
    require ('template/footer.php');