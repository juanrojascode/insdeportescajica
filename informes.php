<?php
    $title = 'Informes' ;
    require ('template/header.php');
?>
	<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
		<div class="container">
			<h1>Estados Financieros<span> A&ntilde;os 2016/2017/2018/2019/2020/2021</span></h1>
		</div>
	</section>

	<section class="bg-breadcrumbs bg-light text-center text-sm-left">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="/">Inicio</a></li>
				<li>Estados Financieros</li>
			</ol>
		</div>
	</section>

	<section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<article class="post-blog">
						<div class="post-img"><img alt="" src="images/documentos3.jpg" /></div>
						
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Noviembre, 2020</a></time><span>&nbsp; Estados Financieros 2020 &nbsp;</span></div>
						
						<div class="post-title">
							<h5 class="text-regular">Remitente: Directora Insdeportes</h5>
						</div>
						
						<div class="post-body">
							<p>El Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute; en cabeza de su directora Katherine Artunduaga Mendoza, da a conocer los estados financieros correspondientes al a&ntilde;o 2020. Dando cumplimiento a la ley de transparencia y acceso a la informaci&oacute;n.</p>
						</div>
					</article>
	
					<article class="post-blog">
						<div class="post-img"><img alt="" src="images/documentos2.jpg" /></div>
						
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Diciembre, 2019</a></time><span>&nbsp; Estados Financieros 2019&nbsp;</span></div>
						
						<div class="post-title">
							<h5 class="text-regular">Remitente: Director Insdeportes</h5>
						</div>
						
						<div class="post-body">
							<p>El Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute; en cabeza del Lic. Fabian Quintero da a conocer los estados financieros correspondientes al a&ntilde;o 2019. Dando cumplimiento a la ley de transparencia y acceso a la informaci&oacute;n.</p>
						</div>
					</article>
	
					<article class="post-blog">
						<div class="post-img"><img alt="" src="images/documentos1.jpg" /></div>
						
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Diciembre, 2018</a></time><span>&nbsp; Estados Financieros 2018 &nbsp;</span></div>
						
						<div class="post-title">
							<h5 class="text-regular">Remitente: Director Insdeportes</h5>
						</div>
						
						<div class="post-body">
							<p>El Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute; en cabeza del Lic. Fabian Quintero da a conocer los estados financieros correspondientes al a&ntilde;o 2019. Dando cumplimiento a la ley de transparencia y acceso a la informaci&oacute;n.</p>
						</div>
					</article>
				</div>
				<div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
					<div class="sidebar text-xs-left">
						<div class="sidebar-module">
							<h5>Año 2022</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2022/Enero-2022.pdf" target="_blank">Informe Enero 2022</a></li>
								<li><a href="files/estados/2022/Febrero-2022.pdf" target="_blank">Informe Febrero 2022</a></li>
							</ul>
							<h5>A&ntilde;o 2021</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2021/Enero 2021.pdf" target="_blank">Informe Enero 2021</a></li>
								<li><a href="files/estados/2021/Febrero 2021.pdf" target="_blank">Informe Febrero 2021</a></li>
								<li><a href="files/estados/2021/Marzo 2021.pdf" target="_blank">Informe Marzo 2021</a></li>
								<li><a href="files/estados/2021/Abril 2021.pdf" target="_blank">Informe Abril 2021</a></li>
								<li><a href="files/estados/2021/Mayo 2021.pdf" target="_blank">Informe Mayo 2021</a></li>
								<li><a href="files/estados/2021/jun-2021.pdf" target="_blank">Informe Junio 2021</a></li>
								<li><a href="files/estados/2021/jul-2021.pdf" target="_blank">Informe Julio 2021</a></li>
								<li><a href="files/estados/2021/ago-2021.pdf" target="_blank">Informe Agosto 2021</a></li>
								<li><a href="files/estados/2021/sep-2021.pdf" target="_blank">Informe Septiembre 2021</a></li>
								<li><a href="files/estados/2021/oct-2021.pdf" target="_blank">Informe Octubre 2021</a></li>
								<li><a href="files/estados/2021/nov-2021.pdf" target="_blank">Informe Noviembre 2021</a></li>
								<li><a href="files/estados/2021/dic-2021.pdf" target="_blank">Informe Diciembre 2021</a></li>
							</ul>
							<h5>A&ntilde;o 2020</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2020/Enero-2020.pdf" title="20 de Marzo de  2018">Informe Enero 2020</a></li>
								<li><a>Informe Febrero 2020</a></li>
								<li><a href="files/estados/2020/Marzo-2020.pdf">Informe Marzo 2020</a></li>
								<li><a href="files/estados/2020/Abril-2020.pdf">Informe Abril 2020</a></li>
								<li><a href="files/estados/2020/Mayo-2020.pdf">Informe Mayo 2020</a></li>
								<li><a href="files/estados/2020/Junio-2020.pdf">Informe Junio 2020</a></li>
								<li><a href="files/estados/2020/Julio-2020.pdf">Informe Julio 2020</a></li>
								<li><a href="files/estados/2020/Agosto-2020.pdf">Informe Agosto 2020</a></li>
								<li><a href="files/estados/2020/Septiembre-2020.pdf">Informe Septiembre 2020</a></li>
								<li><a href="files/estados/2020/Octubre-2020.pdf">Informe Octubre 2020</a></li>
								<li><a href="files/estados/2020/Noviembre-2020.pdf">Informe Noviembre 2020</a></li>
								<li><a href="files/estados/2020/Diciembre-2020.pdf">Informe Diciembre 2020</a></li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2019</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2019/Enero-2019.pdf" title="20 de Marzo de  2018">Informe Enero 2019</a></li>
								<li><a href="files/estados/2019/Febrero-2019.pdf">Informe Febrero 2019</a></li>
								<li><a href="files/estados/2019/Marzo-2019.pdf">Informe Marzo 2019</a></li>
								<li><a href="files/estados/2019/Abril-2019.pdf">Informe Abril 2019</a></li>
								<li><a href="files/estados/2019/Mayo-2019.pdf">Informe Mayo 2019</a></li>
								<li><a href="files/estados/2019/Junio-2019.pdf">Informe Junio 2019</a></li>
								<li><a href="files/estados/2019/Julio-2019.pdf">Informe Julio 2019</a></li>
								<li><a href="files/estados/2019/Agosto-2019.pdf">Informe Agosto 2019</a></li>
								<li><a href="files/estados/2019/Septiembre-2019.pdf">Informe Septiembre 2019</a></li>
								<li><a>Informe Octubre 2019</a></li>
								<li><a>Informe Noviembre 2019</a></li>
								<li><a href="files/estados/2019/Diciembre-2019.pdf">Informe Diciembre 2019</a></li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2018</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2018/Enero-2018.pdf" title="20 de Marzo de  2018">Informe Enero 2018</a></li>
								<li><a href="files/estados/2018/Febrero-2018.pdf">Informe Febrero 2018</a></li>
								<li><a href="files/estados/2018/Marzo-2018.pdf">Informe Marzo 2018</a></li>
								<li><a href="files/estados/2018/Abril-2018.pdf">Informe Abril 2018</a></li>
								<li><a href="files/estados/2018/Mayo-2018.pdf">Informe Mayo 2018</a></li>
								<li><a href="files/estados/2018/Junio-2018.pdf">Informe Junio 2018</a></li>
								<li><a href="files/estados/2018/Julio-2018.pdf">Informe Julio 2018</a></li>
								<li><a href="files/estados/2018/Agosto-2018.pdf">Informe Agosto 2018</a></li>
								<li><a href="files/estados/2018/Septiembre-2018.pdf">Informe Septiembre 2018</a></li>
								<li><a href="files/estados/2018/Octubre-2018.pdf">Informe Octubre 2018</a></li>
								<li><a href="files/estados/2018/Noviembre-2018.pdf">Informe Noviembre 2018</a></li>
								<li><a href="files/estados/2018/Diciembre-2018.pdf">Informe Diciembre 2018</a></li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2017</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2017/Enero-2017.pdf" title="20 de Marzo de  2018">Informe Enero 2017</a></li>
								<li><a href="files/estados/2017/Febrero-2017.pdf">Informe Febrero 2017</a></li>
								<li><a href="files/estados/2017/Marzo-2017.pdf">Informe Marzo 2017</a></li>
								<li><a href="files/estados/2017/Abril-2017.pdf">Informe Abril 2017</a></li>
								<li><a href="files/estados/2017/Mayo-2017.pdf">Informe Mayo 2017</a></li>
								<li><a href="files/estados/2017/Junio-2017.pdf">Informe Junio 2017</a></li>
								<li><a href="files/estados/2017/Julio-2017.pdf">Informe Julio 2017</a></li>
								<li><a href="files/estados/2017/Agosto-2017.pdf">Informe Agosto 2017</a></li>
								<li><a href="files/estados/2017/Septiembre-2017.pdf">Informe Septiembre 2017</a></li>
								<li><a href="files/estados/2017/Octubre-2017.pdf">Informe Octubre 2017</a></li>
								<li><a href="files/estados/2017/Noviembre-2017.pdf">Informe Noviembre 2017</a></li>
								<li><a href="files/estados/2017/Diciembre-2017.pdf">Informe Diciembre 2017</a></li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>A&ntilde;o 2016</h5>
							<ul class="list-unordered-variant-3">
								<li><a href="files/estados/2016/Enero-2016.pdf" title="20 de Marzo de  2018">Informe Enero 2016</a></li>
								<li><a href="files/estados/2016/Febrero-2016.pdf">Informe Febrero 2016</a></li>
								<li><a href="files/estados/2016/Marzo-2016.pdf">Informe Marzo 2016</a></li>
								<li><a href="files/estados/2016/Abril-2016.pdf">Informe Abril 2016</a></li>
								<li><a href="files/estados/2016/Mayo-2016.pdf">Informe Mayo 2016</a></li>
								<li><a href="files/estados/2016/Junio-2016.pdf">Informe Junio 2016</a></li>
								<li><a href="files/estados/2016/Julio-2016.pdf">Informe Julio 2016</a></li>
								<li><a href="files/estados/2016/Agosto-2016.pdf">Informe Agosto 2016</a></li>
								<li><a href="files/estados/2016/Septiembre-2016.pdf">Informe Septiembre 2016</a></li>
								<li><a href="files/estados/2016/Octubre-2016.pdf">Informe Octubre 2016</a></li>
								<li><a href="files/estados/2016/Noviembre-2016.pdf">Informe Noviembre 2016</a></li>
								<li><a href="files/estados/2016/Diciembre-2016.pdf">Informe Diciembre 2016</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
    require ('template/footer.php');