<?php
    $title = 'Seguridad y salud en el trabajo' ;
    require ('template/header.php');
?>

  <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
    <div class="container">
      <h1>Seguridad y Salud en el Trabajo&nbsp;<span>A&ntilde;o 2021 /</span></h1>
    </div>
  </section>
  <section class="bg-breadcrumbs bg-light text-center text-sm-left">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Inicio</a></li>
        <li><a class="text-dark">SST/</a></li>
      </ol>
    </div>
  </section>
  <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-8">
          <article class="post-blog">
            <div class="post-meta-top">
              <time datetime="2022">Febrero 8, 2022</time>
            </div>
            <div class="post-title">
              <h5 class="text-regular">EVALUACION CAPACITACIÓN COVID 19 - 2022</h5>
            </div>
            <div class="post-body">
              <div class="row">
                <div class="col-12 col-md-7">
                  <p>El procedimiento de inducción a los protocolos de bioseguridad es una obligación institucional que busca involucrar a funcionarios y contratistas en la prevención del COVID-19 mientras esté vigente la emergencia sanitaria, en pro de brindarles herramientas que terminan por evitar focos de contagio de origen laboral tanto para los colaboradores como para los usuarios. La capacitación estará compuesta por una presentación y une evaluación que deberán anexar al informe del mes de Febrero de 2021.</p>
                </div>
                <div class="col-12 col-md-5">
                  <div class="d-flex justify-contente-center">
                    <div class="btn-group-vertical">
                      <a href="/files/sst/CAPACITACION-COVID19-FUNCIONARIOSYCONTRATISTAS.pdf" target="_blank" class="btn btn-primary mb-2">Capacitación</a>
                      <a href="https://docs.google.com/forms/d/e/1FAIpQLSeD-eLgExGr5CofpdCcVXM1tMaFhl6pQnL7OMcG4M2q3ZYLBQ/viewform" target="_blank" class="btn btn-success">Evaluación</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </article>

          <article class="post-blog">
            <div class="post-img">
              <div class="embededContent oembed-provider- oembed-provider-youtube" data-align="center" data-maxheight="315" data-maxwidth="560" data-oembed="https://www.youtube.com/watch?v=4fZXQ1S2xTI&amp;t=32s" data-oembed_provider="youtube" data-resizetype="custom" style="text-align: center;"><iframe allowfullscreen="true" allowscriptaccess="always" frameborder="0" height="315" scrolling="no" src="//www.youtube.com/embed/4fZXQ1S2xTI?wmode=transparent&amp;jqoemcache=b490A" width="560"></iframe></div>
            </div>
            <div class="post-meta-top"><strong><time datetime="2016">AGOSTO, 2021</time><span>INDUCCI&Oacute;N EN&nbsp; EL SISTEMA DE GESTI&Oacute;N DE SEGURIDAD Y SALUD EN EL TRABAJO</span></strong></div>
            <div class="post-body">
              <p><strong>El SG-SST y el &aacute;rea de Gesti&oacute;n Humana tiene el gusto de invitarlos al desarrollo de la Inducci&oacute;n en SST, una obligaci&oacute;n institucional que nos permitir&aacute; conocer mas a fondo nuestros derechos y obligaciones frente a la SST.</strong></p>
              <p><strong>Recuerda que debes ver el video y desarrollar la evaluaci&oacute;n antes del 20 de Agosto y anexar la evidencia en el informe de actividades del mes de Agosto de 2021.</strong></p>
              <p><strong>&iexcl;Recuerda que la Seguridad y salud en el trabajo la construimos entre todos!.</strong></p>
            </div>
          </article>

          <article class="post-blog">
            <div class="post-img"><strong><img alt="" src="images/documentos4.jpg" /></strong></div>
            <div class="post-meta-top"><strong><time datetime="2016">Marzo, 2021</time><span>&nbsp; INFOGRAFÍA PLAN DE EMERGENCIAS IMDRC &nbsp;</span></strong></div>
            <div class="post-title">
              <h5 class="text-regular"><strong><a href="files/sst/INFOGRAFÍA PLAN DE EMERGENCIAS IMDRC.pdf">Click Aqu&iacute;</a></strong></h5>
            </div>
            <div class="post-body">
              <p><strong><a href="https://docs.google.com/forms/d/e/1FAIpQLSeB4wqBZ3wjwvRVDVzBxIxa4yU5P0PQylbaERhqEIQu3fwZWg/viewform" target="_blank">CLICK AQU&Iacute; PARA&nbsp;EVALUACI&Oacute;N DE LA INDUCCI&Oacute;N EN SST</a></strong></p>
              <p><strong>El Instituto Municipal de Deporte y Recreaci&oacute;n del Municipio de Cajic&aacute;, en cabeza de su Directora Ana Katherine Artunduaga Mendoza, quiere compartir con contratistas, funcionarios y usuarios la informaci&oacute;n m&aacute;s relevante de nuestro Plan de Emergencias y poder tener las herramientas necesarias que nos permitan reaccionar de manera correcta y oportuna ante una eventual emergencia. &iexcl;&iexcl;Aqu&iacute; encontrar&aacute;s toda la informaci&oacute;n necesaria para responder ante una eventual emergencia‼.</strong></p>
            </div>
          </article>

          <article class="post-blog offset-top-40 offset-sm-top-120">
            <div class="post-img">
              <strong><img alt="" src="images/documentos3.jpg" /></strong>
            </div>
            <div class="post-meta-top">
              <strong><time datetime="2016">Enero, 2021</time><span>&nbsp; Evaluaci&oacute;n Capacitaci&oacute;n Covid &nbsp;</span></strong>
            </div>
            <div class="post-title">
              <h5 class="text-regular">
                <strong><a href="https://docs.google.com/forms/d/e/1FAIpQLScdGDrytS5AljX_lxQAElU0y6lVqckf1Ne2LSRqh6HmDt9PAQ/viewform">Click Aqu&iacute;</a></strong>
              </h5>
            </div>
            <div class="post-body">
              <p><strong>El procedimiento de inducci&oacute;n a los protocolos de bioseguridad es una obligaci&oacute;n institucional que busca involucrar a trabajadores y contratistas en la prevenci&oacute;n del COVID19 mientras est&eacute; vigente la emergencia sanitaria, en pro de brindarles herramientas que terminan por evitar focos de contagio de origen laboral.</strong></p>
            </div>
          </article>
        </div>

        <div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
          <div class="sidebar text-xs-left">
            <div class="sidebar-module">
              <h5><strong>A&ntilde;o 2021</strong></h5>
              <ul class="list-unordered-variant-3">
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a href="files/sst/POLÍTICA DE PREVENCIÓN DE ACOSO LABORAL.pdf" target="_blank">Pol&iacute;tica de Prevenci&oacute;n de Acoso Laboral</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a href="files/sst/POLÍTICA DEL MEDIO AMBIENTE.pdf" target="_blank">Pol&iacute;tica del Medio Ambiente</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a href="files/sst/POLÍTICA DE CERO TOLERANCIA CON ALCOHOL, TABACO Y SPA.pdf" target="_blank">Pol&iacute;tica de Cero Tolerancia con Alcohol, Tabaco y SPA</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a href="files/sst/POLÍTICA DE SEGURIDAD Y SALUD EN EL TRABAJO.pdf" target="_blank">Pol&iacute;tica de Seguridad y Salud en el Trabajo</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a class="text-ebold" href="files/sst/INFOGRAFÍA PLAN DE EMERGENCIAS IMDRC.pdf">Infograf&iacute;a Plan de Emergencias IMDRC</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a class="text-ebold" href="files/sst/CAPACITACION - COVID 19 INGRESO.ppsx">Capacitaci&oacute;n Covid 19 Ingreso</a></strong></span></span></li>
                <li><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><strong><a class="text-ebold" href="files/sst/INSTRUCTIVO CAPACITACIÓN COVID19.pdf">Instructivo Capacitaci&oacute;n Covid 19</a></strong></span></span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php
    require ('template/footer.php');