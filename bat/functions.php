<?php

function dateConvert( $date ){
    $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
    $date = new DateTime( $date );
    $day = $date->format('j');
    $month = $months[$date->format('n')-1];
    $year = $date->format('Y');

    return $month.' '.$day.', '.$year;
}