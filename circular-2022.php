<?php
	$title = 'Circulares 2022' ;
	require ('template/header.php');
?>

    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Circulares</h1>
            <!-- <h1>Planes y Programas<span> A&ntilde;o 2021 /</span></h1> -->
        </div>
    </section>

    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="circulares" class="text-dark">Circulares</a></li>
                <li><a class="text-dark">2022</a></li>
            </ol>
        </div>
    </section>

    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left planes">
        <div class="container py-3">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">CIRCULAR N. 003 INFORMATIVA PROCESO INSCRIPCIONES PROGRAMAS DEPORTIVOS YDE FOMENTO SOCIAL VIGENCIA 2022 PRIMER PERIODO.</h4>
                            <p class="card-text">Por la cual se informan los lineamientos para las inscripciones de los programas deportivos ofertados por el Instituto Municipal de Deporte y Recreación de Cajicá en la vigencia del periodo comprendido entre el 1 de febrero al 30 de junio del 2022.</p>
                            <a href="/files/circulares/2022/Cir-003-2022-21012022173251.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    require ('template/footer.php');