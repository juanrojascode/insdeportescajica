<?php
    $title = 'Noticias' ;
	
    require ('template/header.php');
	include ('bat/functions.php');

	$conn = new PDO($config['db']['conn'], $config['db']['user'], $config['db']['pass'], $config['db']['options']);
	$sentRow = $conn->prepare('SELECT * FROM data_news');
	$sentRow->execute();
	$numRow = $sentRow->rowCount();

	$numItemsForPage = 6;
?>

		<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
			<div class="container">
				<h1 style="text-align: center;">Noticias Insdeportes</h1>
			</div>
		</section>
		<!-- Page Content-->
		<!--Breadcrumbs-->
		<section class="bg-breadcrumbs bg-light text-center text-sm-left">
			<div class="container">
				<ol class="breadcrumb">
					<li style="text-align: center;">Conoce aqu&iacute; todas nuestras noticias y actividades.</li>
				</ol>
			</div>
		</section>
		<!--section blog-->
		<!-- Start Notices -->
		<section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
			<div class="container">
				<div class="row clearleft-custom-1">
					<?php
					if ($numRow > 0) {
						$page = false;
						//examino la pagina a mostrar y el inicio del registro a mostrar
						if (isset($_GET["page"])) {
							$page = $_GET["page"];
						}

						if (!$page) {
							$start = 0;
							$page = 1;
						} else {
							$start = ($page - 1) * $numItemsForPage;
						}
						//calculo el total de paginas
						$total_pages = ceil($numRow / $numItemsForPage);

						$sql = "SELECT * FROM data_news ORDER BY date DESC LIMIT ".$start.", ".$numItemsForPage;
						$sent = $conn->prepare($sql);
						$sent->execute();
						$rowCount = $sent->rowCount();
						
						//Se imprimen los resultados
						if ($rowCount > 0) {
							while ($row = $sent->fetch()) {
								echo '<div class="col-xs-12 col-sm-6 offset-top-40 offset-sm-top-0">
										<article class="post-blog post-blog-mod-1">
										<div class="post-img">
											<iframe width="100%" height="315" src="'.$row['video_url'].'" title="Video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" data-uw-styling-context="true"></iframe>
										</div>
										<div class="post-meta-top">
											<time><span class="text-base">'.dateConvert($row['date']).'</span></time>
										</div>
										<div class="post-title">
											<h5 class="text-regular">'.$row['title'].'</h5>
										</div>
										<div class="post-body">
											<p>'.$row['description'].'</p>
										</div>
										</article>
									</div>';
							}
						}

						echo '<div class="col-xs-12 text-left offset-top-40">';
						echo '<nav class="text-center text-sm-left">';
						echo '<ul class="pagination text-center">';
						if ($total_pages > 1) {
							if ($page != 1) {
								echo '<li><a href="?page='.($page-1).'"><span aria-hidden="true">&laquo;</span></a></li>';
							}
							for ($i=1;$i<=$total_pages;$i++) {
								if ($page == $i) {
									echo '<li class="active"><a href="#">'.$page.'</a></li>';
								} else {
									echo '<li><a href="?page='.$i.'">'.$i.'</a></li>';
								}
							}
							if ($page != $total_pages) {
								echo '<li><a href="?page='.($page+1).'"><span aria-hidden="true">&raquo;</span></a></li>';
							}
						}
						echo '</ul>';
						echo '</nav>';
						echo '</div>';
					}
					?>
				</div>
			</div>
		</section>
		<!-- End Notices -->

<?php
    require ('template/footer.php');