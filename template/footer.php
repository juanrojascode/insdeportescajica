    </main>
    <footer class="page-foot footer-default text-center text-md-left undefined">
        <div class="container">
            <div class="range range-sm-justify">
                <div class="cell-xs-12 cell-md-4 cell-md-push-1 text-md-left">
                    <div class="rd-navbar-brand">
                    <div class="brand-name">
                        <a href="/"><img alt="" height="38" src="images/path590.png" width="171" /></a>
                    </div>
                    </div>
                    <p class="copyright">&copy; | Diseño por Templatemonster <br><a class="text-base" href="files/terminos.pdf">Términos y Condiciones</a></p>
                </div>
                <div class="cell-xs-12 cell-md-4 offset-top-20 offset-md-top-7 offset-lg-top-0 cell-md-push-2">
                    <p><strong>Oficina:</strong><br>
                    <span class="text-base"><?= $config['site']['address'] ?></span></p>
                    <p><strong>Número de contacto:</strong><br><?= $config['site']['phone'] ?></p>
                </div>
                <div class="cell-xs-12 cell-md-4 offset-top-20 offset-md-top-7 offset-lg-top-0 cell-md-push-3 text-md-right">
                    <p><strong>Horario de atención</strong><br><?= $config['site']['scheduleBottom'] ?></p>
                    <p><strong>Correo de notificaciones judiciales</strong><br>
                    <a href="mailto:<?= $config['site']['courtMail'] ?>"><?= $config['site']['courtMail'] ?></a></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Global Mailform Output-->
    <!-- PhotoSwipe Gallery-->
    <div aria-hidden="true" class="pswp" role="dialog" tabindex="-1">
        <div class="pswp__scroll-wrap">
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>

                <div class="pswp__caption">
                    <div class="pswp__caption__cent"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Java script-->
    <script src="js/core.min.js"></script>
    <script src="js/script.js"></script>

    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"), s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/6218eb35a34c245641282ee1/1fsoldi54';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    </body>
</html>