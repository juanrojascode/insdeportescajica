<?php
    //Archivo con la configuración y variables.
    $config = require ('config.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="format-detection" content="telephone=no">
    <link href="images/faviconnew.png" rel="icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!--<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:100,400,800,600,300,700,500">-->

    <!-- Stylesheets-->
    <link href="css/style.css" rel="stylesheet" />

    <!--[if lt IE 10]><div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div><script src="js/html5shiv.min.js"></script><![endif]-->
    <?php
        //Validamos si el title es igual al acompañamiento.
        $retVal = ($title == $config['site']['name']) ? $title : $title .' | '. $config['site']['name'] ;
    ?>
    <title><?= $retVal ?></title>

    <script>(function (d) { var s = d.createElement("script"); s.setAttribute("data-account", "q0RzUOKu1Z"); s.setAttribute("src", "https://cdn.userway.org/widget.js"); (d.body || d.head).appendChild(s); })(document)</script>
    <noscript>Please ensure Javascript is enabled for purposes of <a href="https://userway.org">website accessibility</a></noscript>
</head>

<body class="text-center">

    <main class="page-content">
        <?php require ('template/nav.php'); ?>