<!-- Start Header -->
<header class="page-head header-swiper">
	<!-- Start Navbar -->
	<div class="rd-navbar-wrap header-default">
		<nav class="rd-navbar" data-device-layout="rd-navbar-fixed" data-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth" data-sm-device-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fullwidth" data-stick-up-offset="100px">
			<div class="rd-navbar-inner">
				<button class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-top-panel" type="submit"></button>

				<div class="rd-navbar-top-panel">
					<div class="top-panel-inner-left">
						<dl class="dl-horizontal-variant-1 text-white">
							<dd>
								<span class="material-icons-location_on icon"></span>
								<a class="text-white" href="https://goo.gl/maps/mmJTUCxWs2RpudcHA"><?= $config['site']['address'] ?></a>
							</dd>
						</dl>

						<dl class="dl-horizontal-variant-1 text-white">
							<dd>
								<span class="material-icons-local_phone icon"></span>
								<a class="text-white" href="callto:<?= $config['site']['phone'] ?>"><?= $config['site']['phone'] ?></a>
							</dd>
						</dl>
						
						<dl class="dl-horizontal-variant-1">
							<dd>
								<span class="material-icons-event icon"></span>
								<span><?= $config['site']['scheduleTop'] ?></span>
							</dd>
						</dl>						
					</div>
				</div>

				<!-- Start Navbar Panel-->
				<div class="rd-navbar-panel-wrapper">
					<div class="rd-navbar-panel">
						<!-- RD Navbar Toggle-->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                        <!-- RD Navbar Brand-->
						<div class="rd-navbar-brand">
							<a class="brand-name" href="/"><img alt="" height="43" src="images/logo-insdeportes.png" width="165"/></a>
						</div>
					</div>

					<div class="nav-wrapper">
						<div class="rd-navbar-nav-wrap">
							<div class="logo-inner">
								<!-- RD Navbar Brand-->
								<div class="rd-navbar-brand">
									<a class="brand-name" href="/"><img alt="" src="images/logo.png"/></a>
								</div>
							</div>

							<!-- Start navbar -->
							<ul class="rd-navbar-nav">
								<li>
									<a href="/">Inicio</a>
								</li>
								<li>
									<a href="/transparencia">Transparencia</a>
								</li>
								<li>
									<a href="/bienestar">Bienestar</a>
								</li>
								<li>
									<a href="/escueladeportiva">Escuela Deportiva</a>
								</li>
								<li>
									<a href="/prensa">Prensa</a>
								</li>
								<li>
									<a href="/contacto">PQRS/Contáctenos</a>
								</li>
							</ul>
                            <!-- End navbar -->
						</div>
					</div>
				</div>
                <!-- End Navbar Panel-->
			</div>
		</nav>
	</div>
    <!-- End Navbar -->
</header>
<!-- End Header -->