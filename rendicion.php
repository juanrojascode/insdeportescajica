<?php
  $title = 'Rendición' ;
  require ('template/header.php');
?>

  <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
    <div class="container">
      <h1>Rendici&oacute;n de Cuentas<span> A&ntilde;o 2021/ </span></h1>
    </div>
  </section>
  <section class="bg-breadcrumbs bg-light text-center text-sm-left">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Inicio</a></li>
        <li><a class="text-dark">Derechos de Petici&oacute;n /</a></li>
      </ol>
    </div>
  </section>
  <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-8">
          <article class="post-blog">
            <div class="post-img"><img alt="" src="images/documentos3.jpg" /></div>
            <div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Marzo, 2021</a></time><span>&nbsp; Respuesta Derecho de Petici&oacute;n An&oacute;nimo &nbsp;</span></div>
            <div class="post-title">
              <h5 class="text-regular"><a href="rendicion">Remitente: Directora Insdeportes</a></h5>
            </div>
            <div class="post-body">
              <p>Padres Deportistas Insdeportes Cajic&aacute;</p>
            </div>
          </article>
        </div>
        <div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
          <div class="sidebar text-xs-left">
            <div class="sidebar-module">
              <h5>A&ntilde;o 2021</h5>
              <ul class="list-unordered-variant-3">
                <li><a class="text-ebold" href="files/derechodepeticion/RESPUESTA DERECHO PETICION 0265.pdf" title="Inscripciones 1-2021">Respuesta Derecho de Petici&oacute;n 0265</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php
  require ('template/footer.php');