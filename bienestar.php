<?php
    $title = 'Bienestar' ;
    require ('template/header.php');
?>

	<section class="section-55 section-lg-top-115 section-lg-bottom-0 text-sm-left">
		<div class="container">
			<h1 style="text-align: center;">Bienestar al 100%</h1>
			
			<p></p>
		
			<div class="row offset-top-40">
				<div class="col-xs-12">
					<div class="responsive-tabs responsive-accordion-variant-1" data-type="accordion">
						<ul class="resp-tabs-list">
							<li class="h5 text-regular">Integridad</li>
							<li class="h5 text-regular">Bienestar</li>
							<li class="h5 text-regular">Capacitaci&oacute;n</li>
							<li class="h5 text-regular">Bioseguridad</li>
							<li class="h5 text-regular">Seguridad y Salud</li>
						</ul>
					
						<div class="resp-tabs-container">
							<div>
								<ul>
									<li><a href="files/CODIGO DE INTEGRIDAD.pdf" target="_blank">C&oacute;digo de Integridad</a></li>
								</ul>
							</div>	
							<div>
								<ul>
									<li><a href="files/Plan_bienestar.pdf" target="_blank">Plan de Bienestar e Incentivos</a> <!-- RD Navbar Dropdown--></li>
								</ul>
							</div>	
							<div>
								<ul>
									<li class="h6 text-ebold text-base text-uppercase"><a href="files/Plan_capacitacion.pdf" target="_blank">Plan de Capacitaci&oacute;n</a></li>
								</ul>
							</div>	
							<div>
								<ul>
									<li><a href="protocolosbioseguridad">Protocolos de Bioseguridad</a></li>
								</ul>
							</div>	
							<div>
								<ul>
									<li><a href="sst" target="_blank">Seguridad y Salud en el Trabajo</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
    require ('template/footer.php');