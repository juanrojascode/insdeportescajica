<?php
    $title = 'Archivos' ;
    require ('template/header.php');
?>

	<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
		<div class="container">
			<h1>Actos Administrativos<span>Resoluciones e Informativos</span></h1>
		</div>
	</section>

	<section class="bg-breadcrumbs bg-light text-center text-sm-left">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="/">Inicio</a></li>
				<li><a class="text-dark" href="#">Archivos</a></li>
				<li class="active">A&ntilde;os 2018, 2019, 2020, 2021, 2022</li>
			</ol>
		</div>
	</section>

	<section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<article class="post-blog">
						<!-- <div class="post-img" style="text-align: justify;"></div> -->
						<div class="post-meta-top">
							<time datetime="2016">
								<a class="text-base" href="#">Diciembre 29, 2021</a>
							</time>
							<span>&nbsp; Notificación &nbsp;</span>
						</div>
						<div class="post-title">
							<h5 class="text-regular">Lista definitiva aspirantes admitidos a entrevista convocatoria correspondiente al nombramiento del cargo Jefe (a) de Control Interno</h5>
						</div>
						<div class="post-body">
							<p>Una vez surtida la etapa de reclamaciones y resueltas las mismas, acorde a lo dispuesto en el art&iacute;culo 26 de la Resolución 170 del 14 de diciembre de 2021; el Instituto Municipal de Deporte y Recreación de Cajic&aacute; realiza la publicación de la lista definitiva de admitidos a la entrevista se&ntilde;alada en el art&iacute;culo 27 de la resolución citada, para la convocatoria y nombramiento del cargo Jefe (a) de Control Interno en el Instituto de Deportes y Recreación de Cajic&aacute; periodo 2022 - 2025.</p>
						</div>
					</article>
					<article>
						<div class="post-meta-top">
							<time datetime="2016"><a class="text-base" href="#">Diciembre 27, 2021</a></time><span>&nbsp; Notificación &nbsp;</span>
						</div>
						<div class="post-title">
							<h5 class="text-regular"><a href="files/2021/LISTA DEFINITIVA ASPIRANTES ADMITIDOS A ENTREVISTA.pdf" target="_blank">Click Aqu&iacute; Lista definitiva aspirantes admitidos a entrevista convocatoria correspondiente al nombramiento del cargo Jefe (a) de Control Interno</a></h5>
						</div>
					</article>
					<article>
						<div class="post-title">
							<h5 class="text-regular">Evaluación y valoración de estudios y experiencia adicional a los requisitos m&iacute;nimos</h5>
						</div>
						<div class="post-body">
							<p>En cumplimiento de lo dispuesto en la Resolución N&deg; 170 de 2021, &nbsp;y conforme al cronograma dispuesto en la Resolución 175 de 2021, la entidad procede a publicar los resultados de las evaluaciones de valoración de estudios y experiencia adicional a los requisitos m&iacute;nimos de los postulados a la convocatoria correspondiente al nombramiento del cargo jefe(a) de control interno en el Instituto Municipal de Deportes y Recreación de Cajic&aacute;, para el periodo 2022 &ndash; 2025.</p>
						</div>
					</article>
					<article>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Diciembre 23, 2021</a></time><span>&nbsp; Notificación &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular"><a href="files/2021/Listado de resultados prueba de conocimientos.pdf" target="_blank">Click Aqu&iacute; </a><a href="files/2021/EVALUACIÓN Y VALORACIÓN DE ESTUDIOS Y EXPERIENCIA ADICIONAL A LOS REQUISITOS MINIMOS.pdf" target="_blank">Evaluación y valoración de estudios y experiencia adicional a los requisitos m&iacute;nimos</a></h5>
						</div>
					</article>
					<article>
						<div class="post-title">
							<h5 class="text-regular">Listado de resultados prueba de conocimientos</h5>
						</div>
						<div class="post-body">
							<p>En cumplimiento de lo dispuesto en la Resolución 175 de 2021 que modifica la Resolución N&deg; 170 y 171 de 2021, la entidad procede a publicar los resultados de las pruebas de conocimiento de la convocatoria correspondiente al nombramiento del cargo jefe(a) de control interno en el Instituto Municipal de Deportes y Recreación de Cajic&aacute;, para el periodo 2022 &ndash; 2025.</p>
						</div>
					</article>
					<article>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Diciembre 22, 2021</a></time><span>&nbsp; Notificación &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular"><a href="files/2021/Listado de resultados prueba de conocimientos.pdf" target="_blank">Click Aqu&iacute; para conocer el listado de resultados de la prueba de conocimientos</a></h5>
						</div>
					</article>
					<article>
						<div class="post-title">
							<h5 class="text-regular">Resolución 175 del 22 de diciembre de 2021</h5>
						</div>
						<div class="post-body">
							<p>Con el fin de dar cumplimiento a lo establecido en la Resolución N&deg; 170 de 2021 modificada parcialmente por la Resolución 171 de 2021, la entidad procede a publicar la Resolución 175 del 22 de diciembre de 2021, la cual modifica el cronograma de la convocatoria correspondiente al nombramiento del cargo jefe(a) de control interno en el Instituto Municipal de Deportes y Recreación de Cajic&aacute;, para el periodo 2022 &ndash; 2025.</p>
						</div>
					</article>
					<article>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Diciembre 20, 2021</a></time><span>&nbsp; Notificación &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular"><a href="files/2021/RESOLUCION 175.pdf" target="_blank">Click Aqu&iacute; Resolución 175 de diciembre de 2021</a></h5>
						</div>
					</article>
					<article>
						<div class="post-title">
							<h5 class="text-regular">Listado de admitidos y no admitidos</h5>
						</div>
						<div class="post-body">
							<p>En cumplimiento de la Resolución N&deg; 170 de 2021 modificada parcialmente por la Resolución 171 de 2021, la entidad procede a publicar la lista de admitidos y no admitidos en la convocatoria correspondiente al nombramiento del cargo jefe(a) de control interno en el Instituto Municipal de Deportes y Recreación de Cajic&aacute;, para el periodo 2022 &ndash; 2025.</p>
						</div>
					</article>
					<article>
						<div class="post-title">
							<h5 class="text-regular"><a href="files/2021/Lista de admitidos y no admitidos a la convocatoria.pdf" target="_blank">Click Aqu&iacute; para conocer el listado de admitidos y no admitidos</a></h5>
						</div>
						<div class="post-body">
							<p><img alt="" src="images/documentos1.jpg" /></p>
						</div>
					</article>
					<article>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Junio, 2020</a></time><span>&nbsp; Notificación &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular">Remitente: Directora Insdeportes</h5>
						</div>
						<div class="post-body">
							<p>El Instituto Municipal de Deporte y Recreación de Cajic&aacute; en cabeza de Ana Katherine Artunduaga Mendosa da a conocer a trav&eacute;s de este medio las circulares informativas, resoluciones o actos administrativos concernientes a temas que afecten a Servidores p&uacute;blicos, Contratistas, Instructores del Instituto Municipal de Deporte y Recreación de Cajic&aacute; , Clubes deportivos y comunidad en general..</p>
						</div>
					</article>
					<article class="post-blog">
						<div class="post-img"><img alt="" src="images/documentos2.jpg" /></div>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Septiembre 22, 2020</a></time><span>&nbsp; Resolución 58 &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular">Remitente: Directora Insdeportes</h5>
						</div>
						<div class="post-body">
							<p>Por la cual se reglamenta el uso de los escenarios deportivos y recreativos del municipio de cajic&aacute;, en cumplimiento de &ntilde;as medidas adoptadas por el presidente de la rep&uacute;blica median el decreto 1168 de 2020, con relación al distanciamiento individual responsable.</p>
						</div>
					</article>
					<article class="post-blog">
						<div class="post-img"><img alt="" src="images/documentos3.jpg" /></div>
						<div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Agosto 20, 2020</a></time><span>&nbsp; Resolución 50 &nbsp;</span></div>
						<div class="post-title">
							<h5 class="text-regular">Remitente: Directora Insdeportes</h5>
						</div>
						<div class="post-body">
							<p>Por medio del cual se establecen los requisitos para el otorgamiento de la certificación de cumplimiento de requisitos de los deportistas de alto rendimiento en cumplimiento del decreto municipal 108 de 2020 por parte del Instituto Municipal de Deporte y Recreación de Cajic&aacute;.</p>
						</div>
					</article>
				</div>

				<div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
					<div class="sidebar text-xs-left">
						<div class="sidebar-module">
							<h5>Archivos 2022</h5>
							<ul class="list-unordered-variant-3">
								<li><a class="text-ebold" href="files/2022/resoluciones/RESOLUCION_048.pdf"><img src="<?= $config['site']['icon-pdf'] ?>" style="width:20px" />Resolución 48</a></li>
							</ul>
							<h5>Archivos 2021</h5>
							<ul>
								<li style="text-align: justify;"><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 175.pdf" target="_blank">Resolución 175</a></span></span></strong></li>
								<li style="text-align: justify;"><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 171.pdf" target="_blank">Resolución 171</a></span></span></strong></li>
								<li><a href="files/2021/FORMULARIO DE INSCRIPCIÓN RESOLUCION 170.pdf" target="_blank"><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;">Formulario Inscripción Resolución 170</span></span></strong></a></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 170.pdf" target="_blank">Resolución 170</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 168 MODIFICA RESOLUCION 160.pdf" target="_blank">Resolución 168</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 160.pdf" target="_blank">Resolución 160</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 148.pdf" target="_blank">Resolución 148</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 110.pdf" target="_blank">Resolución 110</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 082.pdf" target="_blank">Resolución 082</a></span></span></strong></li>
								<li><strong><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><a href="files/2021/RESOLUCION 055.pdf" target="_blank">Resolución 055</a></span></span></strong></li>
								<li><a class="text-ebold" href="files/2021/RESOLUCION 054.pdf" target="_blank"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;"><strong>Resolución 054</strong></span></span></a></li>
								<li><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;"><a class="text-ebold" href="files/2021/RESOLUCIÓN 048.pdf" target="_blank">Resolución 048</a></span></span></strong></li>
								<li><a href="files/2021/RESOLUCION 044.pdf" target="_blank"><font face="verdana, geneva, sans-serif"><span style="font-size: 14px;"><b>Resolución 044</b></span></font></a></li>
								<li><a href="files/2021/RESOLUCION 041.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 041</span></span></strong></a></li>
								<li><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;"><a href="files/2021/RESOLUCION 040.pdf" target="_blank">Resolución 040</a></span></span></strong></li>
								<li><a href="files/2021/RESOLUCION 038.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 038</span></span></strong></a></li>
								<li><a href="files/2021/RESOLUCION 028.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 028</span></span></strong></a></li>
								<li><a href="files/2021/RESOLUCION 026.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 026</span></span></strong></a></li>
								<li><a href="files/2021/RESOLUCION 022.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 022</span></span></strong></a></li>
								<li><a href="files/2021/RESOLUCION 09.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 09</span></span></strong></a></li>
								<li><a href="files/2021/RESOLUCION 005.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 005</span></span></strong></a></li>
								<li><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;"><a href="files/2021/RESOLUCION 004.pdf" target="_blank">Resolución 004</a></span></span></strong></li>
								<li><a href="files/2021/RESOLUCION 003.pdf" target="_blank"><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;">Resolución 003</span></span></strong></a></li>
								<li><strong><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size:14px;"><a href="files/2021/RESOLUCION 01.pdf" target="_blank">Resolución 01</a></span></span></strong></li>
							</ul>
							<h5>Archivos 2020</h5>
							<ul class="list-unordered-variant-3">
								<li><a class="text-ebold" href="files/2020/Resolución-58.pdf">Resolución 58</a></li>
								<li><a class="text-ebold" href="files/2020/resolución-reapertura-final.pdf">Resolución 53</a></li>
								<li><a class="text-ebold" href="files/2020/RESOLUCIOìN-CERTIFICACIOìN-DECRETO-108-DE-20201.pdf">Resolución 50</a></li>
								<li><a class="text-ebold" href="files/2020/Resolucion-No.-41-de-2020-Adopcion-.pdf">Resolución 41</a></li>
								<li><a class="text-ebold" href="files/2020/Resoluciones-040-Incremento.pdf">Resolución 40</a></li>
								<li><a class="text-ebold" href="files/2020/resolucioìn-no-39.pdf">Resolución 39</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-38.pdf">Resolución 38</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-37.pdf">Resolución 37</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-33-de-2020-Insdeportes.pdf">Resolución 33</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-27-del-2020.pdf">Resolución 27</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-26-de-2020-modifica-presupuesto.pdf">Resolución 26</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-aprobación-contratacion-directa.pdf">Resolución 23.1</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-15-2020.pdf">Resolución 15</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-13-2020.pdf">Resolución 13</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-12-de-2020.pdf">Resolución 12</a></li>
								<li><a class="text-ebold" href="files/2020/Resoluciòn-11-2020.pdf">Resolución 11.1</a></li>
								<li><a class="text-ebold" href="files/2020/Reolucion-10-2020.pdf">Resolución 10</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-08-plan-adquisiciones.pdf">Resolución 8</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-223-Reglamento-Sistema-Abierto-.pdf">Resolución 223</a></li>
								<li><a class="text-ebold" href="files/2020/Res-209-Cod-Integridad.1.pdf">Resolución 209</a></li>
								<li><a class="text-ebold" href="files/2020/Res-197-de-2020-Escala-de-Viaticos.pdf">Resolución 197</a></li>
								<li><a class="text-ebold" href="files/2020/Res-133-2019-Comité-Institucional-de-Gestión.pdf">Resolución 133</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-12-2019-Escala-de-Viaticos.pdf">Resolución 12</a></li>
								<li><a class="text-ebold" href="files/2020/Res-151.1-Codigo-Disciplinarios-de-Torneos.pdf">Resolución 151.1</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-151-2018-Cod-Disciplinari.pdf">Resolución 151</a></li>
								<li><a class="text-ebold" href="files/2020/Resolución-146-2018-Apoyos.pdf">Resolución 146</a></li>
							</ul>
						</div>
						<div class="sidebar-module">
							<h5>Archivos 2019</h5>
							<ul class="list-unordered-variant-3">
								<li><a class="text-ebold" href="files/2019/Resolucion-_03012019152515.pdf">Resolución 03</a></li>
								<li><a class="text-ebold" href="files/2019/Resolucion-127.pdf">Resolución 127</a></li>
								<li><a class="text-ebold" href="files/2019/Circular-interna.pdf">Circular Interna 003</a></li>
								<li><a class="text-ebold" href="files/2019/Resolucion-festivales-de-ciclismo.pdf">Resolución 161</a></li>
								<li><a class="text-ebold" href="files/2019/resolucion-285.pdf">Resolución 285</a></li>
							</ul>
						</div>

						<div class="sidebar-module">
							<h5>Archivos 2018</h5>

							<ul class="list-unordered-variant-3">
								<li><a class="text-ebold" href="files/2018/circular-informativa-003.pdf" title="20 de Marzo de  2018">Circular Informativa 003</a></li>
								<li><a class="text-ebold" href="files/2018/resolucion-146.pdf">Resolución 146</a></li>
								<li><a class="text-ebold" href="files/2018/resolucion_203.pdf">Resolución 203</a></li>
								<li><a class="text-ebold" href="files/2018/RESOLUCIÓN-juegos-superate-2018-con-firma.pdf">Resolución 212.1</a></li>
								<li><a class="text-ebold" href="files/2018/RESOLUCION-246-2018.pdf">Resolución 246</a></li>
								<li><a class="text-ebold" href="files/2018/Respuesta.pdf">Respuesta 753</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion-Intercolegiados-No.-03-final.pdf">Resolución 259</a></li>
								<li><a class="text-ebold" href="files/2018/RESOLUCION-N°363.pdf">Resolución 363</a></li>
								<li><a class="text-ebold" href="files/2018/RESPUESTA-OFICOS-RADICADOS-1060-1083-AMC-IMRD-747-2018.pdf">Respuesta 747</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion-399.pdf">Resolución 399</a></li>
								<li><a class="text-ebold" href="files/2018/RESOLUCION-DE-APERTURA-SA-MC-002-DE-2018.pdf">Resolución 439</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion-450.pdf">Resolución 450</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion_14122018115949.pdf">Resolución 454</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion-443.pdf">Resolución 443</a></li>
								<li><a class="text-ebold" href="files/2018/Resolucion-444.pdf">Resolución 444</a></li>
								<li><a class="text-ebold" href="files/2018/Resolución-462.pdf">Resolución 462</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php
    require ('template/footer.php');