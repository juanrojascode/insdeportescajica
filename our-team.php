<?php
    $title = 'Nuestro equipo' ;
    require ('template/header.php');
?>

  <section class="bg-breadcrumbs bg-light text-center text-sm-left">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="/">Inicio</a></li>
        <li><a class="text-dark" href="#">P&aacute;ginas</a></li>
        <li class="active">Nuestro Equipo</li>
      </ol>
    </div>
  </section>

  <section class="section-55 section-lg-top-115 section-lg-bottom-0 text-sm-left">
  <div class="container">
  <h1>Directora</h1>

  <div class="row offset-top-40">
  <div class="col-xs-12 col-sm-6 col-md-4 text-sm-left offset-top-60 offset-sm-top-0">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-2.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Katherine Artunduaga</h5>
  <span class="h6">Administradora de Instituciones de Servicio</span>

  <p></p>

  <div class="media-body">
  <p>Hice parte del consejo consultivo de mujeres del municipio, hago parte del liderazgo en ense&ntilde;anza y pr&aacute;ctica del evangelismo en tomados de la mano de Dios, visitando fundaciones, motivando a los j&oacute;venes a que se alejen de los vicios, y en hospitales llevando esperanza en las crisis de enfermedad, as&iacute; como tambi&eacute;n, en la resoluci&oacute;n de conflictos personales y familiares</p>
  </div>

  <ul class="list-inline offset-top-25">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
  </div>
  </article>
  </div>
  </div>
  </div>
  </section>

  <section class="section-55 section-lg-top-115 section-lg-bottom-0 text-sm-left">
  <div class="container">
  <h1>Funcionarios</h1>

  <div class="row offset-top-40">
  <div class="col-xs-12 col-sm-6 col-lg-3 text-sm-left">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-1.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Shirley Jimenez Rodriguez</h5>
  <span class="h6">Administradora de Empresas</span>

  <p></p>

  <div class="media-body">
  <p>Perfil Profesional...</p>
  </div>

  <ul class="list-inline offset-top-25">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
  </div>
  </article>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3 text-sm-left offset-top-60 offset-sm-top-0">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-5.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Martha Yaneth Albornoz Sanabria</h5>
  <span class="h6">Abogada- Especialista en Derecho Administrativo y Penal</span>

  <p></p>

  <div class="media-body">
  <p>Me considero una persona anal&iacute;tica, con criterio, honesta, independiente y proactiva; con facilidad de aprendizaje y comunicaci&oacute;n intergrupal, con altos niveles de exigencia y compromiso en la obtenci&oacute;n de metas. En el sector p&uacute;blico he prestado mis servicios durante 12 a&ntilde;os aproximadamente, acreditando experiencia en el campo del Derecho Administrativo en asuntos de car&aacute;cter constitucional, administrativo, procesal, disciplinario, contractual y fiscal.</p>
  </div>

  <ul class="list-inline offset-top-25">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
  </div>
  </article>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3 text-sm-left offset-top-60 offset-lg-top-0">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-3.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Edwin Antonio Casas Bello</h5>
  <span class="h6">Ingeniero Industrial</span>

  <p></p>

  <div class="media-body">
  <p>Perfil Profesional...</p>
  </div>

  <ul class="list-inline offset-top-25">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
  </div>
  </article>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3 text-sm-left offset-top-60 offset-lg-top-0">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-4.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Jairo Enrrique Lara</h5>
  <span class="h6">Operario de Mantenimiento</span>

  <p></p>

  <div class="media-body">
  <p>Curso en Instituto Juan Don Bosco de soldadura, Oficios varios en mantenimioento de las instalaciones</p>
  </div>

  <ul class="list-inline offset-top-25">
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </ul>
  </div>
  </article>
  </div>
  </div>
  </div>
  </section>

  <section class="section-55 section-lg-top-115 section-lg-bottom-0 text-sm-left">
  <div class="container">
  <div class="row offset-top-40">
  <div class="col-xs-12 col-sm-6 col-lg-3 text-sm-left">
  <article class="thumbnail-variant-1">
  <div class="img-wrap"><img alt="" src="images/team-6.jpg" /></div>

  <div class="caption offset-top-30">
  <h5>Yenny Carolina Bello Clavijo</h5>
  <span class="h6">Contadora P&uacute;blica</span>

  <p></p>

  <div class="media-body">
  <p>Cajique&ntilde;a, comprometida con la gesti&oacute;n financiera del Insdeportes Cajic&aacute;, realizando el manejo de recursos p&uacute;blicos, contabilidad, presupuesto y tesorer&iacute;a de manera transparente, organizada y &eacute;tica, buscando la mejora cont&iacute;nua en cada uno de los procesos a cargo.</p>
  </div>

  </div>
  </article>
  </div>
</div>
  </div>
  </section>

<?php
    require ('template/footer.php');