<?php
    $title = 'Recursos' ;
    require ('template/header.php');
?>

    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Recursos</h1>
        </div>
    </section>

    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="transparencia" class="text-dark">Transparencia</a></li>
                <li class="active">Recursos</li>
            </ol>
        </div>
    </section>

    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="post-blog post-blog-mod-2">
                        <div class="post-body">
                            <h3 class="text-center">¿Qué es el lenguaje claro?</h3>
                            <p>El lenguaje claro es una forma de simplificar la información pública, especialmente la relacionada con trámites y servicios con el fin de que los ciudadanos puedan comprender sin intermediarios y para que ejerzan efectivamente sus derechos y deberes, este no es hablar de manera coloquial o infantil, ni concibe al otro como un sujeto incapaz de aprender o de investigar, ni suprime la rigurosidad de nuestro trabajo como servidores públicos.</p>
                            <p>El lenguaje claro permite encontrarnos y sintonizarnos en la misma frecuencia y hablar el mismo idioma, porque a veces pareciera que la hablamos al ciudadano como si fuera experto en asuntos públicos.</p>
                            <p>El lenguaje claro promueve la reducción del uso de intermediarios, aumenta la eficiencia en la gestión de las solicitudes de los ciudadanos, promueve la transparencia y el acceso a la información, facilita el control y la participación ciudadana y fomenta la inclusión social para grupos con discapacidad.</p>
                            <h6>Información tomada de: <a href="https://www.dnp.gov.co/programa-nacional-del-servicio-al-ciudadano/Paginas/Lenguaje-Claro.aspx" target="_blank">https://www.dnp.gov.co/programa-nacional-del-servicio-al-ciudadano/Paginas/Lenguaje-Claro.aspx</a></h6>
                            <p>A continuación, se muestran unos cortos videos con algunos tips sobre el lenguaje claro:</p>
                            <div class="row" style="margin-top: 1rem">
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/q7Ap1tvbF8U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/MoskcZ0H810" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 1rem">
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/QTRAHg1ZjQw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/q6koLaSxgik" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 1rem">
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/9-gJlJeeb_E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/pINjFpVN6zA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
                                </div>
                            </div>
                            <p class="text-center"><a href="https://forms.gle/EtqfQX5J3qjWPwJP6" class="btn btn-success">Evaluación tips<br> Lenguaje Claro</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    require ('template/footer.php');