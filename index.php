<?php
    $title = 'Insdeportes Cajicá' ;
    require ('template/header.php');
?>
    <!-- Start swiper-slider -->
    <div class="swiper-container swiper-slider">
        <div class="swiper-wrapper">
            <div class="swiper-slide" data-slide-bg="images/slide-1-1.webp">
                <div class="swiper-slide-caption">
                    <div class="container text-left" data-caption-animate="fadeInUp">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6">
                                <!--<h2 class="bg-light">Inscripciones</h2>-->
                                <!-- <p class="h5 text-regular"><br class="visible-lg" />Click en el botón</p> -->
                                <a class="btn btn-transparent btn-sm btn-icon btn-icon-right" href="https://appinsdeportes.gov.co" target="_blank"><span>Clic aquí</span></a>
                            </div>
                        </div>
                    </div>
                </div>t
            </div>
            <div class="swiper-slide" data-slide-bg="images/slide-4.webp">
                <div class="swiper-slide-caption">
                    <div
                        class="container text-left"
                        data-caption-animate="fadeInUp"
                    >
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-7 col-lg-6">
                                <h2>
                                    Solicitud<br class="visible-lg" />

                                    de Carnets
                                </h2>

                                <p class="h5 text-regular">
                                    <br class="visible-lg" />

                                    Para Escenarios Deportivos
                                </p>

                                <a
                                    class="btn btn-transparent btn-sm btn-icon btn-icon-right"
                                    href="https://forms.gle/GWaG16goV9FD9dB77"
                                    target="_blank"
                                    ><span>Solicitar Carnet</span> &nbsp;
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide" data-slide-bg="images/slide-3.webp">
                <div class="swiper-slide-caption">
                    <div class="container text-left">
                        <div class="row">
                            <div
                                class="col-xs-12 col-sm-9 col-md-7 col-lg-6"
                                data-caption-animate="fadeInUp"
                            >
                                <h2>
                                    Encuentra este<br class="visible-lg" />

                                    contenido
                                </h2>

                                <p class="h5 text-regular">
                                    En nuestra<br class="visible-lg" />

                                    p&aacute;gina de facebook
                                </p>

                                <a
                                    class="btn btn-transparent btn-sm btn-icon btn-icon-right"
                                    href="https://www.facebook.com/CajicaInsdeportes/live_videos/"
                                    target="_blank"
                                    ><span>ir al sitio</span> &nbsp;
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Swiper Pagination-->
        <div class="swiper-pagination"></div>
    </div>
    <!-- End swiper-slider -->
    <!-- Start Buttons -->
    <div class="range range-no-gutter">
        <div class="cell-xs-12 cell-sm-6 cell-md-3">
            <a href="https://youtu.be/ey8Pox1mWuo" target="_blank">
                <div class="banner-block border-right">
                    <p class="h6 text-bold">
                        PASO A PASO <br>INSCRIPCIONES
                    </p>
                </div>
            </a>
        </div>
        <div class="cell-xs-12 cell-sm-6 cell-md-3">
            <a href="files/2021/RESOLUCION 168 MODIFICA RESOLUCION 160.pdf" target="_blank">
                <div class="banner-block border-right">
                    <p class="h6 text-bold">
                        RESOLUCIÓN <br>MEJORES DEL AÑO
                    </p>
                </div>
            </a>
        </div>
        <div class="cell-xs-12 cell-sm-6 cell-md-3">
            <a href="files/2022/PROGRAMACIONSEGUNDAPARADACAJICA.pdf" target="_blank">
                <div class="banner-block">
                    <p class="h6 text-bold">
                    RESULTADOS 2DA PARADA DPTAL <br>TENIS DE MESA 2022
                    </p>
                </div>
            </a>
        </div>
        <div class="cell-xs-12 cell-sm-6 cell-md-3">
            <a href="https://tustiempos.com/carrera-atletica-ciudad-de-cajica/" target="_blank">
                <div class="banner-block">
                    <p class="h6 text-bold" style="text-align: center">
                        RESULTADOS <br>CARRERA ATLÉTICA 2021
                    </p>
                </div>
            </a>
        </div>
    </div>
    <!-- End Buttons -->
    <!-- Section Motivational phrases -->
    <section class="section-55 section-sm-top-90 section-sm-bottom-0 text-sm-left bg-index-1" >
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-7 col-lg-offset-5" >
                    <h1>
                        Frases<br class="visible-lg" />
                        de motivaci&oacute;n deportiva
                    </h1>

                    <p class="inset-lg-right-90">
                        <span style="color: #000000"
                            >&quot;Odi&eacute; cada minuto de entrenamiento,
                            pero dije: No abandones. Sufre ahora y vive el resto
                            de tu vida como un campe&oacute;n&quot;. Esta frase
                            del gran campe&oacute;n Kasius Klay, Muhammad Ali
                            explica lo complicado que es entrenar cada
                            d&iacute;a y entender que el esfuerzo diario es el
                            &uacute;nico que te va a llevar al
                            &eacute;xito.</span
                        >
                    </p>

                    <ul class="list-marked-variant-1 h5 offset-top-50">
                        <li>
                            <a class="text-primary text-regular"
                                >Nunca te dije que ser&iacute;a f&aacute;cil,
                                pero s&iacute; que al conseguirlo valdr&iacute;a
                                la pena</a
                            >
                        </li>

                        <li>
                            <a class="text-primary text-regular"
                                >La forma de conectar un objetivo con un
                                sue&ntilde;o es a trav&eacute;s de la
                                disciplina</a
                            >
                        </li>

                        <li>
                            <a class="text-primary text-regular"
                                >Si no abandonas, llegar&aacute;s m&aacute;s
                                lejos, y entonces sabr&aacute;s lo lejos que
                                puedes llegar</a
                            >
                        </li>

                        <li>
                            <a class="text-primary text-regular"
                                >Las estad&iacute;sticas est&aacute;n para
                                romperse. Los r&eacute;cords para superarse</a
                            >
                        </li>

                        <li>
                            <a class="text-primary text-regular">Lo importante no es que te derriben, sino que después puedas levantarte</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Motivational phrases -->
    <!-- Section Activities -->
    <section class="section-55 section-sm-top-115 section-sm-bottom-0 text-center">
        <div class="container">
            <div class="heading-variant-1">
                <h1>Actividades de la Semana</h1>
            </div>
        </div>

        <div class="post-img">
            <div class="post-img">
                <a href="https://www.facebook.com/CajicaInsdeportes" target="_blank"><img alt="RENDI CUENTAS 2021 INSDEPORTES CAJICA." src="images/RENDICUENTAS2021INSDEPORTESCAJICA.jpg"/></a>
            </div>
        </div>
    </section>
    <!-- Section Activities -->
    <!-- Section Videos -->
    <section class="rd-parallax">
        <div class="rd-parallax-layer" data-lg-speed="0.4" data-speed="0" data-type="media" data-url="images/parallaxnew.jpg"></div>

        <div class="rd-parallax-layer" data-lg-speed="0.1" data-speed="0" data-type="html">
            <div class="parallax-inner">
                <h1>Videos</h1>
                <p class="h4 text-regular">Publicaciones, Actividades</p>
                <a class="btn btn-white btn-sm btn-icon btn-icon-right offset-top-40" href="https://web.facebook.com/pg/CajicaInsdeportes/videos/?ref=page_internal" target="_blank"><span>videos en Facebook</span></a>
            </div>
        </div>
    </section>
    <!-- Section Videos -->
    <!-- Section Feed IG -->
    <section class="section-55 section-sm-top-115 section-sm-bottom-115 text-center">
        <div class="container">
            <div class="heading-variant-4">
                <p></p>

                <h1>Galeria de Imágenes</h1>
            </div>
        </div>

        <div class="range range-condensed offset-top-40" data-photo-swipe-gallery="gallery">
            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-1_original.jpg"
                    ><img alt="" src="images/insta-1.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-2_original.jpg"
                    ><img alt="" src="images/insta-2.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-3_original.jpg"
                    ><img alt="" src="images/insta-3.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-4_original.jpg"
                    ><img alt="" src="images/insta-4.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-5_original.jpg"
                    ><img alt="" src="images/insta-5.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="817x1089"
                    href="images/insta-6_original.jpg"
                    ><img alt="" src="images/insta-6.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="1200x800"
                    href="images/insta-7_original.jpg"
                    ><img alt="" src="images/insta-7.jpg"
                /></a>
            </div>

            <div class="cell-xs-12 cell-sm-6 cell-md-3">
                <a
                    class="img-thumbnail-variant-3"
                    data-photo-swipe-item=""
                    data-size="800x1200"
                    href="images/insta-8_original.jpg"
                    ><img alt="" src="images/insta-8.jpg"
                /></a>
            </div>
        </div>
        <a class="btn btn-transparent btn-sm btn-icon btn-icon-right offset-top-20 offset-md-top-40" href="https://instagram.com/insdeportescajica/?hl=en" target="_blank"><span>Más Imágenes</span>
        </a>
    </section>
    <!-- Section Feed IG -->
    <!-- Start Frame Google Maps -->
    <iframe allowfullscreen="" aria-hidden="false" frameborder="0" height="280" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1181.8180714755495!2d-74.02934568374741!3d4.9151743202164395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e40777d4d31ccb9%3A0x291c586e250df146!2sColiseo%20Fortaleza%20De%20Piedra%20Cajica!5e0!3m2!1ses!2sco!4v1607961243122!5m2!1ses!2sco" style="border: 0" tabindex="0" width="100%"
    ></iframe>
    <!-- End Frame Google Maps -->
<?php
    require ('template/footer.php');