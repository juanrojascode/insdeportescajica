<?php
    $title = 'PQRS/Contacto' ;
    require ('template/header.php');
?>

	<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
		<div class="container">
			<h1>Contáctanos<span>! Déjanos saber de tí !</span></h1>
		</div>
	</section>


	<section class="section-55 section-lg-top-115 section-lg-bottom-125 text-sm-left">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-lg-6">
					<h5>Ponte en Contacto</h5>
					<!-- RD Mailform-->
					<form action="bat/rd-mailform.php" class="rd-mailform text-left row" data-form-output="form-output-global" data-form-type="contact" method="post">
						<div class="col-xs-12 col-lg-6">
							<div class="form-group">
								<label class="form-label" for="contact-name">Nombres</label>
								<input class="form-control" data-constraints="@Required" id="contact-name" name="name" placeholder="Nombres" type="text" />
							</div>
						</div>
						<div class="col-xs-12 col-lg-6">
							<div class="form-group">
								<label class="form-label" for="contact-phone">Teléfono</label>
								<input class="form-control" data-constraints="@Required @Numeric" id="contact-phone" name="phone" placeholder="Teléfono" type="text" />
							</div>
						</div>
						<div class="col-xs-12">
							<div class="form-group">
								<label class="form-label" for="contact-message">Mensaje</label>
								<textarea class="form-control" data-constraints="@Required" id="contact-message" name="message" placeholder="Escribe tú mensaje aquí"></textarea>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<label class="form-label" for="contact-email-2">E-Mail</label>
								<input class="form-control" data-constraints="@Required @Email" id="contact-email-2" name="email" placeholder="E-Mail" type="email" />
							</div>
						</div>
						<div class="col-xs-12 col-sm-12">
							<div class="form-group">
								<button class="btn btn-transparent btn-sm btn-icon btn-icon-right offset-sm-top-35" type="submit"><span>enviar mensaje</span></button>
							</div>
						</div>
					</form>
				</div>

				<div class="col-xs-12 col-md-4 col-lg-5 col-lg-offset-1 offset-top-55 offset-lg-top-0">
					<address>
						<p>
							<dl>
								<dt class="text-uppercase text-ebold text-dark">Dirección</dt>
								<dd>Calle 1 Sur # 7-56, Coliseo Fortaleza de Piedra, Cajicá, Cundinamarca</dd>
								
							</dl>
						</p>
						<p>
							<dl class="offset-top-30 offset-top-50">
								<dt class="text-uppercase text-ebold text-dark">Horarios</dt>
								<dd>Lunes a Jueves: 8:00 a.m a 1:00 pm - 2:00 pm a 5:30 pm</dd>
								<dd>Viernes: 8:00 am a 1:00 pm - 2:00 pm a 4:30 pm</dd>
							</dl>
						</p>
						<p>
							<dl class="offset-top-30 offset-top-50">
								<dt class="text-uppercase text-ebold text-dark">Teléfonos</dt>
								<dd><a class="text-base" href="callto:6018898746">889 8746</a></dd>
							</dl>
						</p>
						<p>
							<dl class="offset-top-30 offset-top-50">
								<dt class="text-uppercase text-ebold text-dark">email Información</dt>
								<dd><a href="mailto:ventanillaunica@insdeportescajica.gov.co">ventanillaunica@insdeportescajica.gov.co</a></dd>
							</dl>
						</p>
						<p>
							<dl class="offset-top-30 offset-top-50">
								<dt class="text-uppercase text-ebold text-dark">email Notificaciones Judiciales</dt>
								<dd><a href="mailto:juridica@insdeportescajica.gov.co">juridica@insdeportescajica.gov.co</a></dd>
							</dl>
						</p>
						<p>
							<dl class="offset-top-30 offset-top-50">
								<dt class="text-uppercase text-ebold text-dark">tratamiento de datos personales aviso de privacidad</dt>
								<dd><a href="files/terminos.pdf">términos y condiciones</a></dd>
							</dl>
						</p>
					</address>
				</div>
			</div>
		</div>
	</section>

	<iframe allowfullscreen="" aria-hidden="false" frameborder="0" height="280" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1181.8180714755495!2d-74.02934568374741!3d4.9151743202164395!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e40777d4d31ccb9%3A0x291c586e250df146!2sColiseo%20Fortaleza%20De%20Piedra%20Cajica!5e0!3m2!1ses!2sco!4v1607961243122!5m2!1ses!2sco" style="border:0;" tabindex="0" width="1920"></iframe>

<?php
    require ('template/footer.php');