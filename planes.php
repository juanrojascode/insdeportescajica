<?php
    $title = 'Planes y programas' ;
    require ('template/header.php');
?>    
    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
      <div class="container">
        <h1>Planes y Programas</h1>
        <!-- <h1>Planes y Programas<span> A&ntilde;o 2021 /</span></h1> -->
      </div>
    </section>
    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
      <div class="container">
        <ol class="breadcrumb">
          <li><a href="/">Inicio</a></li>
          <li><a class="text-dark">Planes y Programas/</a></li>
        </ol>
      </div>
    </section>
    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-8">
            <article class="post-blog">
              <div class="post-img"><img alt="" src="images/documentos3.jpg" /></div>

              <div class="post-meta-top"><time datetime="2016"><a class="text-base" href="#">Enero 12,
                    2021</a></time><span>&nbsp; Resoluci&oacute;n No. 005 &nbsp;</span></div>

              <div class="post-title">
                <h5 class="text-regular"><a href="files/planes/RES 005 PAC 2021.pdf">Remitente: Directora
                    Insdeportes</a></h5>
              </div>

              <div class="post-body">
                <p>POR LA CUAL SE APRUEBA EL PLAN ANUAL MENSUALIZADO DE CAJA &ndash;PAC- DELINSTITUTO MUNICIPAL DE
                  DEPORTE Y RECREACION DE CAJICA, PARA LA VIGENCIA FISCAL 2021.</p>
              </div>
            </article>
          </div>

          <div class="col-xs-12 col-md-3 col-md-offset-1 offset-top-40 offset-md-top-0">
            <div class="sidebar text-xs-left">
              <div class="sidebar-module">
                <h5>Planes</h5>
                <ul class="list-unordered-variant-3">
                  <li><a href="plan-2021" class="text-ebold">A&ntilde;o 2021</a></li>
                  <li><a href="plan-2022" class="text-ebold">A&ntilde;o 2022</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
<?php
    require ('template/footer.php');