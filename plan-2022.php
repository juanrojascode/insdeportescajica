<?php
    $title = 'Planes del 2022' ;
    require ('template/header.php');
?>
    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Planes y Programas</h1>
            <!-- <h1>Planes y Programas<span> A&ntilde;o 2021 /</span></h1> -->
        </div>
    </section>
    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="planes" class="text-dark">Planes y Programas</a></li>
                <li><a class="text-dark">2022</a></li>
            </ol>
        </div>
    </section>
    <!--section blog-->
    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left planes">
        <div class="container py-3">
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANTICORRUPCIÓN Y DE ATENCIÓN AL CIUDADANO</h4>
                            <p class="card-text">Establecer el mecanismo de control y seguimiento de los
                                factores de riesgo de corrupción, a través de la participación ciudadana, el
                                acceso a la información y la transparencia de las actuaciones del Instituto
                                Municipal de Deporte y Recreación de Cajicá.</p>
                            <a href="/files/2022/planes/PLANANTICORRUPCIONYDEATENCIOONALCIUDADANO.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANUAL DE ADQUISICIONES</h4>
                            <p class="card-text">Se constituye en un instrumento de programación y planificación, al convertirse en una herramienta que apoya el control de la gestión de la administración. La información que recopila el plan anual de adquisiciones es la base que le permite a la entidad tomar decisiones respecto a los elementos que requiere y su priorización, permitiendo cumplir en forma adecuada su misión, visión y objetivos trazados a través del Plan de Desarrollo y el Plan Operativo Anual de Inversiones.</p>
                            <a href="/files/2022/planes/PLANANUALDEADQUISICIONES.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANUAL DE VACANTES - PLAN DE PREVENSIÓN DE RECURSOS HUMANOS</h4>
                            <p class="card-text">El presente plan tiene como punto de partida el análisis descriptivo de la estructura administrativa y su estado actual de la planta de personal describiendo las situaciones administrativas prevista en la ley como en: provisionalidad, encargo y sin proveer, todas por nivel jerárquico, con el fin de proveer un adecuado instrumento de gestión estratégica.</p>
                            <a href="/files/2022/planes/PLANANUALDEVACANTES-PLANDEPREVENSIONDERECURSOSHUMANOS.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE TRATAMIENTO DE RIESGOS DE SEGURIDAD Y DE PRIVACIDAD DE LA INFORMACIÓN</h4>
                            <p class="card-text">El Instituto Municipal de Deporte y Recreación de Cajicá, entendiendo la importancia que es el llevar una adecuada gestión de la información, se desarrolla el plan de tratamiento de riesgos de seguridad y privacidad de la información, el cual permita tener un control para proteger y si es necesario reducir los daños en caso perdidas y daños en la información que tiene el instituto.</p>
                            <a href="/files/2022/planes/PLANDETRATAMIENTODERIESGOSDESEGURIDADYDEPRIVACIDADDELAINFORMACION.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ESTRATÉGICO TECNOLOGÍAS DE LA INFORMACIÓN</h4>
                            <p class="card-text">El Instituto Municipal de Deporte y Recreación de Cajicá a través del plan estratégico de tecnologías de la información (PETI) pretende establecer con este documento una herramienta que permita realizar un análisis y diagnostico de la situación actual de la entidad en el área de informática, definiendo una serie de necesidades de servicios, aplicaciones e infraestructura para la gestión eficiente de cada proceso institucional; Esto implica la evaluación de: Hardware, Software, Redes, Telecomunicaciones, Gestión de Tl y Seguridad Informática.</p>
                            <a href="/files/2022/planes/PLANESTRATEGICOTECNOLOGIIASDELAINFORMACION.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN INSTITUCIONAL DE ARCHIVO - PINAR 2022</h4>
                            <p class="card-text">El Plan Institucional de Archivos - PINAR en el Instituto Municipal de Deporte y Recreación de Cajicá hace parte de los instrumentos de planificación para las diferentes áreas que componen el Instituto, con respecto a la gestión de documentos y la administración de archivos.</p>
                            <p class="card-text">Este, define los lineamientos de las actividades que se llevaran a cabo en el año, estableciendo con la ley 594 del 2000: Archivo general de la nación. El Plan Institucional de archivos identifica los planes, programas y proyectos de la función de archivo de acuerdo con las necesidades, debilidades, riesgos y oportunidades que presente la entidad.</p>
                            <a href="/files/2022/planes/PLANINSTITUCIONALDEARCHIVO-PINAR2022.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ANUAL DE SEGURIDAD Y SALUD EN EL TRABAJO</h4>
                            <p class="card-text">El presente documento, se elabora coma una herramienta práctica, clara y concisa para la implementación, mantenimiento y mejora del sistema de gestión de seguridad y salud en el trabajo durante el año 2022. De esa manera, los colaboradores evidenciando el presente plan coma soporte documental, tendrán claridad sobre las actividades y los documentos que respaldan en general a todo el sistema.</p>
                            <a href="/files/2022/planes/PLANANUALDESEGURIDADYSALUDENELTRABAJO.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN INSTITUCIONAL DE BIENESTAR E INCENTIVOS</h4>
                            <p class="card-text">El Programa de Bienestar Social e Incentivos del Instituto Municipal
                                de Deporte y Recreación de Cajicá, tiene como objetivo principal facilitar la construcción de un clima laboral positive para sus servidores y exaltar la labor que estos realizan en el Instituto Municipal de Deporte y Recreación de Cajicá; a través de diversos espacios y actividades que
                                contribuyan al mejoramiento de su calidad de vida, el desarrollo integral y el sentido de pertenencia con Insdeportes.</p>
                            <a href="/files/2022/planes/PLANINSTITUCIONALDEBIENESTAREINCENTIVOS.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN DE CAPACITACIÓN 2022</h4>
                            <p class="card-text">Tiene como objetivo que los colaboradores del Instituto Municipal de Deporte y Recreación de Cajicá, adquieran conocimientos y habilidades propias de su labor que aporten en la ejecución de su trabajo (perfil) y el desarrollo de sus procesos a cargo. Así mismo, desde la ejecución del plan de capacitación y formación se fomenta la mejora aptitudinal y la actitud de servicio tanto del cliente interno como externo, aportando a la sana convivencia y a mejorar el clima organizacional.</p>
                            <a href="/files/2022/planes/PLANDECAPACITACION2022.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col-md-4">
                        <img src="https://cdn4.iconfinder.com/data/icons/file-extension-names-vol-8/512/24-256.png">
                    </div>
                    <div class="col-md-8 px-3">
                        <div class="card-block px-3">
                            <h4 class="card-title">PLAN ESTRATEGICO DE TALENTO HUMANO 2022</h4>
                            <p class="card-text">El Instituto Municipal de Deporte y Recreación de Cajicá, mediante la implementación del Modelo Integrado de planeación y gestión – MIPG, en su política de “Gestión de talento Humano”, concibe a los funcionarios públicos como el activo más relevante para la institución, siendo ellos quienes, con el desempeño de su labor, aportan al cumplimiento de los objetivos organizacionales, garantizando así, una atención efectiva en todos los procesos a desarrollar.</p>
                            <a href="/files/2022/planes/PLANESTRATEGICODETALENTOHUMANO2022.pdf" target="_blank" class="btn btn-primary">Ver PDF</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page Footer-->
<?php
    require ('template/footer.php');