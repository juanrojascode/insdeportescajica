<?php
    $title = 'Misión y visión' ;
    require ('template/header.php');
?>
	<section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
		<div class="container">
			<h1>Nuestro Instituto</h1>
		</div>
	</section>

	<section class="bg-breadcrumbs bg-light text-center text-sm-left">
	<div class="container">
	<ol class="breadcrumb">
		<li><a href="/">Inicio</a></li>
		<li><a class="text-dark" href="#">P&aacute;ginas</a></li>
		<li class="active">Misi&oacute;n, Visi&oacute;n, Pol&iacute;ticas y Valores</li>
	</ol>
	</div>
	</section>

	<section class="section-55 section-lg-top-115 section-lg-bottom-125 text-sm-left">
	<div class="container">
	<h1>Misi&oacute;n</h1>

	<div class="row">
	<div class="col-xs-12">
	<dl class="terms-list"><!-- <dt class="h4 text-uppercase">General Information</dt>-->
		<dd class="text-sm-left">Somos una entidad descentralizada de orden Municipal encargada del dise&ntilde;o, planeaci&oacute;n, estructuraci&oacute;n y ejecuci&oacute;n de las politicas deportivas del Municipio de Cajic&aacute;, a trav&eacute;s del fomento y la masificaci&oacute;n en las pr&aacute;cticas del deporte, la recreaci&oacute;n, la actividad fisica y el aprovechamiento del tiempo libre conforme a las disposiciones de Ley: con criterios de inclusi&oacute;n social, sostenibilidad econ&oacute;mica, ambiental y tejido social para la paz en busca de un reconocimiento y aceptaci&oacute;n a nivel municipal, regional y nacional.</dd>
	</dl>
	</div>
	</div>
	</div>
	&nbsp;

	<div class="container">
	<h1>Visi&oacute;n</h1>

	<div class="row">
	<div class="col-xs-12">
	<dl class="terms-list">
		<dd class="text-sm-left">Para el a&ntilde;o 2035 el Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute;, ser&aacute; una entidad lider en los procesos de preparaci&oacute;n integral del deportista, impulsando la competencia sana con criterios de inclusi&oacute;n, masificaci&oacute;n y mejora continua en la participaci&oacute;n de las competencias nacionales e internacionales.</dd>
	</dl>
	</div>
	</div>
	</div>
	&nbsp;

	<div class="container">
	<h1>Pol&iacute;ticas y Valores</h1>

	<div class="row">
	<div class="col-xs-12">
	<dl class="terms-list">
		<dt class="h4 text-uppercase">Pol&iacute;tica de Calidad</dt>
		<dd class="text-sm-left">El Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute; est&aacute; comprometido con la gesti&oacute;n transparente, efectiva y participativa del fomento del deporte, planeando. coordinando, ejecutando y controlando con eficiencia, eficacia y efectividad los programas, planes y proyectos encaminados al desarrollo de las actividades l&uacute;dicas. recreativas y deportivas dentro de los diferentes grupos que conforman la comunidad, contribuyendo de esta forma al mejoramiento de la calidad de vida de la poblaci&oacute;n Cajique&ntilde;a, de conformidad con los lineamientos legales en materia, en b&uacute;squeda de la mejora continua y permanente, de la mano de un talento humano competente y calificado, con apoyo de la alta direcci&oacute;n, garantizando un servicio m&aacute;s &aacute;gil y oportuno al ciudadano.</dd>
		<dt class="h4 text-uppercase">Objetivos de Calidad</dt>
		<dd class="text-sm-left">1.Consolidar la Estrategia T&eacute;cnica Deportiva Eficaz, Eficiente y Efectiva para preparar deportistas de alto nivel competitivo, desde la orientaci&oacute;n del Sistema Nacional delDeporte; garantizando los principios, ejes trasversales y lineas Estrat&eacute;gicas de la Politica P&uacute;blica Nacional y Lineamientos de Deporte y Recreaci&oacute;n en el Plan de Desarrollo Municipal.</dd>
		<dd class="text-sm-left">2. Desarrollar una gesti&oacute;n transparente mediante el manejo adecuado de los recursos econ&oacute;micos y de la informaci&oacute;n tendiente a la consolidaci&oacute;n y sostenibilidad del Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute;.</dd>
		<dd class="text-sm-left">3. Establecer los mecanismos que permitan el fomento, masificaci&oacute;n, desarrollo y pr&aacute;ctica del deporte, la recreaci&oacute;n, la actividad fisica y el aprovechamiento del tiempo libre, mediante la integraci&oacute;n en espacios de convivencia y paz como herramienta de transformaci&oacute;n inclusi&oacute;n y equidad social</dd>
		<dd class="text-sm-left">4. Promover programas que orienten el desarrollo de las pr&aacute;cticas extraescolar complementaria para ni&ntilde;os, ni&ntilde;as y j&oacute;venes con el fin de aprovechar el tiempo libre. 5. Generar y brindar a la comunidad oportunidades de participaci&oacute;n en procesos formativos de iniciaci&oacute;n fundamentaci&oacute;n y perfeccionamiento como contribuci&oacute;n al desarrollo integral de los ni&ntilde;os y las ni&ntilde;as en el fomento de una cultura fisica para el mejoramiento de la calidad de vida.</dd>
		<dd class="text-sm-left">6. Mejorar continuamente en el sistema integral de gesti&oacute;n, en cuanto a su eficacia y eficiencia, acorde a los avances de la norma.</dd>
		<dd class="text-sm-left">7. Contar con un equipo de trabajo enfocado al cumplimiento de metas, comprometido y con visi&oacute;n de crecimiento en conjunto que permita la ejecuci&oacute;n de procedimientos encaminados a la mejora continua.</dd>
		<dd class="text-sm-left">8. Generar estrategias deportivas para promover la competitividad, preparaci&oacute;n y apoyo a los deportistas de altos logros, estableciendo criterios t&eacute;cnicos aplicados en los organismos del deporte asociado, que componen el Sistema Nacional del Deporte a nivel municipal</dd>
		<dd class="text-sm-left">9. Dise&ntilde;ar, desarrollar e implementar la Politica Půblica del Deporte en el Municipio de Cajic&aacute; como herramienta transversal de la pr&aacute;ctica deportiva.</dd>
		<dt class="h4 text-uppercase">Principios Corporativos</dt>
		<dd class="text-sm-left">1. <strong>Transparencia</strong>: Una de las herramientas fundamentales para que el ciudadano pueda informarse a trav&eacute;s de los diferentes mecanismos de participaci&oacute;n con el fin de garantizar el adecuado uso de los recursos econ&oacute;micos y de la eficacia y eficiencia de la administraci&oacute;n p&uacute;blica.</dd>
		<dd class="text-sm-left">2. <strong>Igualdad</strong>: El Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute; est&aacute; comprometido con la creaci&oacute;n de condiciones de igualdad reales y efectivas para satisfacer las necesidades de la poblaci&oacute;n.</dd>
		<dd class="text-sm-left">3. <strong>Tolerancia</strong>: Se establece como una base fundamental de nuestras pol&iacute;ticas, por medio de excelentes relaciones interpersonales para garantizar la satisfacci&oacute;n de las necesidades de la comunidad cajique&ntilde;a.</dd>
		<dd class="text-sm-left">4. <strong>Respeto</strong>: Como esencia de las actuaciones humanas, criterio de valoraci&oacute;n y de inclusi&oacute;n dentro de los procesos del Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute;.</dd>
		<dd class="text-sm-left">5. <strong>Eficiencia</strong>: Estableciendo objetivos organizacionales claros con la participaci&oacute;n activa de los miembros del Instituto Municipal de Deporte y Recreaci&oacute;n de Cajic&aacute;, permitiendo as&iacute; una ejecuci&oacute;n adecuada de costos sin la afectaci&oacute;n al patrimonio.</dd>
	</dl>
	<a class="btn btn-primary btn-sm" href="contacto">cont&aacute;ctanos</a></div>
	</div>
	</div>
	</section>
<?php
    require ('template/footer.php');