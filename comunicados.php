<?php
    $title = 'Comunicados' ;
    require ('template/header.php');
?>

    <section class="text-sm-left section-40 section-md-top-75 section-md-bottom-115">
        <div class="container">
            <h1>Comunicados</h1>
        </div>
    </section>

    <!--Breadcrumbs-->
    <section class="bg-breadcrumbs bg-light text-center text-sm-left">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li><a href="bienestar" class="text-dark">Bienestar</a></li>
                <li class="active">Bioseguridad</li>
            </ol>
        </div>
    </section>

    <section class="section-55 section-lg-top-125 section-lg-bottom-125 text-sm-left">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="post-blog post-blog-mod-2">
                        <div class="post-body">
                            <p>Comunicado sobre atención al público el viernes 18 de marzo de 2022, por conmemoración de los 485 años de fundación del municipio de Cajicá.</p>
                            <p class="text-center">
                                <a href="files/2022/comunicados/comunicado-17032022.pdf"><img src="<?= $config['site']['icon-pdf'] ?>" alt="Icono de PDF" class="icon-pdf ms-0">Comunicado - 17 de marzo de 2022</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
    require ('template/footer.php');